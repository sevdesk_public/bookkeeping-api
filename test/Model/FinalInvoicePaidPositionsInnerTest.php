<?php
/**
 * FinalInvoicePaidPositionsInnerTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Accounting - Bookkeeping API
 *
 * Welcome to the Bookkeeping API of Team Plumbus
 *
 * The version of the OpenAPI document: 20.0.0
 * Contact: team_ap@sevdesk.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.2.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace OpenAPI\Client\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * FinalInvoicePaidPositionsInnerTest Class Doc Comment
 *
 * @category    Class
 * @description FinalInvoicePaidPositionsInner
 * @package     OpenAPI\Client
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class FinalInvoicePaidPositionsInnerTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "FinalInvoicePaidPositionsInner"
     */
    public function testFinalInvoicePaidPositionsInner()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "datevRevenueAccountNumber"
     */
    public function testPropertyDatevRevenueAccountNumber()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "taxRate"
     */
    public function testPropertyTaxRate()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "totals"
     */
    public function testPropertyTotals()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "totalsClaim"
     */
    public function testPropertyTotalsClaim()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
