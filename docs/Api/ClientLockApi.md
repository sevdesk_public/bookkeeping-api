# OpenAPI\Client\ClientLockApi

All URIs are relative to http://localhost:8080, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiV1InternalRebookingStatusGet()**](ClientLockApi.md#apiV1InternalRebookingStatusGet) | **GET** /api/v1/_internal/rebooking/status | This endpoint is used to receive the status of a client lock for a specific client. |


## `apiV1InternalRebookingStatusGet()`

```php
apiV1InternalRebookingStatusGet($sDTracingId): \OpenAPI\Client\Model\ClientLockStatusResponse
```

This endpoint is used to receive the status of a client lock for a specific client.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\ClientLockApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string

try {
    $result = $apiInstance->apiV1InternalRebookingStatusGet($sDTracingId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientLockApi->apiV1InternalRebookingStatusGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\ClientLockStatusResponse**](../Model/ClientLockStatusResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
