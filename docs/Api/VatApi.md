# OpenAPI\Client\VatApi

All URIs are relative to http://localhost:8080, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**getAllowedTaxRatesBy()**](VatApi.md#getAllowedTaxRatesBy) | **GET** /api/v1/_internal/accounting/countryvatrates | Retrieve valid EU VAT rates for a specific country and date |


## `getAllowedTaxRatesBy()`

```php
getAllowedTaxRatesBy($countryCode, $date): \OpenAPI\Client\Model\VatRate[]
```

Retrieve valid EU VAT rates for a specific country and date

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\VatApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$countryCode = new \OpenAPI\Client\Model\CountryCodeEnum(); // CountryCodeEnum | The ISO 3166-1 alpha-2 country code for which VAT rates are to be retrieved.
$date = 2024-12-05; // string | The date for which the VAT rates are to be retrieved. The date must be in ISO 8601 format.

try {
    $result = $apiInstance->getAllowedTaxRatesBy($countryCode, $date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VatApi->getAllowedTaxRatesBy: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **countryCode** | [**CountryCodeEnum**](../Model/.md)| The ISO 3166-1 alpha-2 country code for which VAT rates are to be retrieved. | |
| **date** | **string**| The date for which the VAT rates are to be retrieved. The date must be in ISO 8601 format. | |

### Return type

[**\OpenAPI\Client\Model\VatRate[]**](../Model/VatRate.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
