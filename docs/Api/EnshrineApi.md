# OpenAPI\Client\EnshrineApi

All URIs are relative to http://localhost:8080, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiV1InternalBookkeepingBookingEnshrineDaterangePatch()**](EnshrineApi.md#apiV1InternalBookkeepingBookingEnshrineDaterangePatch) | **PATCH** /api/v1/_internal/bookkeeping/booking/enshrine/daterange | Use this endpoint when bookings of documents, attachments and payments are to be enshrined by date range |
| [**apiV1InternalBookkeepingBookingEnshrineDocumentsPatch()**](EnshrineApi.md#apiV1InternalBookkeepingBookingEnshrineDocumentsPatch) | **PATCH** /api/v1/_internal/bookkeeping/booking/enshrine/documents | Use this endpoint when bookings of documents, attachments and payments are to be enshrined |


## `apiV1InternalBookkeepingBookingEnshrineDaterangePatch()`

```php
apiV1InternalBookkeepingBookingEnshrineDaterangePatch($sDTracingId, $enshrineBookingsDateRange): \OpenAPI\Client\Model\EnshrineResponse
```

Use this endpoint when bookings of documents, attachments and payments are to be enshrined by date range

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnshrineApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$enshrineBookingsDateRange = new \OpenAPI\Client\Model\EnshrineBookingsDateRange(); // \OpenAPI\Client\Model\EnshrineBookingsDateRange

try {
    $result = $apiInstance->apiV1InternalBookkeepingBookingEnshrineDaterangePatch($sDTracingId, $enshrineBookingsDateRange);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnshrineApi->apiV1InternalBookkeepingBookingEnshrineDaterangePatch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **enshrineBookingsDateRange** | [**\OpenAPI\Client\Model\EnshrineBookingsDateRange**](../Model/EnshrineBookingsDateRange.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\EnshrineResponse**](../Model/EnshrineResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalBookkeepingBookingEnshrineDocumentsPatch()`

```php
apiV1InternalBookkeepingBookingEnshrineDocumentsPatch($sDTracingId, $enshrineDocuments): \OpenAPI\Client\Model\EnshrineResponse
```

Use this endpoint when bookings of documents, attachments and payments are to be enshrined

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnshrineApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$enshrineDocuments = new \OpenAPI\Client\Model\EnshrineDocuments(); // \OpenAPI\Client\Model\EnshrineDocuments

try {
    $result = $apiInstance->apiV1InternalBookkeepingBookingEnshrineDocumentsPatch($sDTracingId, $enshrineDocuments);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnshrineApi->apiV1InternalBookkeepingBookingEnshrineDocumentsPatch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **enshrineDocuments** | [**\OpenAPI\Client\Model\EnshrineDocuments**](../Model/EnshrineDocuments.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\EnshrineResponse**](../Model/EnshrineResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
