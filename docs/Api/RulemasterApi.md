# OpenAPI\Client\RulemasterApi

All URIs are relative to http://localhost:8080, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiV1InternalAccountingBookingAccountingrecordDocumentGet()**](RulemasterApi.md#apiV1InternalAccountingBookingAccountingrecordDocumentGet) | **GET** /api/v1/_internal/accounting/booking/accountingrecord/document | Use this endpoint to get all accounting records of a document. |
| [**apiV1InternalAccountingReportRulemasterUstvaGet()**](RulemasterApi.md#apiV1InternalAccountingReportRulemasterUstvaGet) | **GET** /api/v1/_internal/accounting/report/rulemaster/ustva | Returns the UStVA report for given period as rulemaster including document relations per category. This is only internal functionality for QA not for customer. |
| [**apiV1InternalBookkeepingBookingImmutabilityReversedDaterangePatch()**](RulemasterApi.md#apiV1InternalBookkeepingBookingImmutabilityReversedDaterangePatch) | **PATCH** /api/v1/_internal/bookkeeping/booking/immutability/reversed/daterange | Use this endpoint when the immutability of bookings of documents and payments have to be reversed by date range |
| [**apiV1InternalBookkeepingBookingImmutabilityReversedDocumentsPatch()**](RulemasterApi.md#apiV1InternalBookkeepingBookingImmutabilityReversedDocumentsPatch) | **PATCH** /api/v1/_internal/bookkeeping/booking/immutability/reversed/documents | Use this endpoint when the immutability of bookings of documents and payments have to be reversed. |
| [**apiV1InternalRebookingDocumentTypeDocumentIdGet()**](RulemasterApi.md#apiV1InternalRebookingDocumentTypeDocumentIdGet) | **GET** /api/v1/_internal/rebooking/{documentType}/{documentId} | This endpoint is used to trigger a rebooking for a specific document. |
| [**findAllBookingKeysBy()**](RulemasterApi.md#findAllBookingKeysBy) | **GET** /api/v1/_internal/accounting/booking/rulemaster/bookingkey | Use this endpoint to fetch all Rulemaster Booking Keys for display in View |


## `apiV1InternalAccountingBookingAccountingrecordDocumentGet()`

```php
apiV1InternalAccountingBookingAccountingrecordDocumentGet($documentId, $documentType): \OpenAPI\Client\Model\DocumentAccountingRecordResult[]
```

Use this endpoint to get all accounting records of a document.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RulemasterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$documentId = 456; // int
$documentType = Invoice; // string

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingrecordDocumentGet($documentId, $documentType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RulemasterApi->apiV1InternalAccountingBookingAccountingrecordDocumentGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **documentId** | **int**|  | |
| **documentType** | **string**|  | |

### Return type

[**\OpenAPI\Client\Model\DocumentAccountingRecordResult[]**](../Model/DocumentAccountingRecordResult.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingReportRulemasterUstvaGet()`

```php
apiV1InternalAccountingReportRulemasterUstvaGet($startMonth, $endMonth): string
```

Returns the UStVA report for given period as rulemaster including document relations per category. This is only internal functionality for QA not for customer.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RulemasterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$startMonth = 2022-07; // string
$endMonth = 'endMonth_example'; // string

try {
    $result = $apiInstance->apiV1InternalAccountingReportRulemasterUstvaGet($startMonth, $endMonth);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RulemasterApi->apiV1InternalAccountingReportRulemasterUstvaGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **startMonth** | **string**|  | |
| **endMonth** | **string**|  | [optional] |

### Return type

**string**

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalBookkeepingBookingImmutabilityReversedDaterangePatch()`

```php
apiV1InternalBookkeepingBookingImmutabilityReversedDaterangePatch($sDTracingId, $enshrineBookingsDateRange): \OpenAPI\Client\Model\EnshrineResponse
```

Use this endpoint when the immutability of bookings of documents and payments have to be reversed by date range

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RulemasterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$enshrineBookingsDateRange = new \OpenAPI\Client\Model\EnshrineBookingsDateRange(); // \OpenAPI\Client\Model\EnshrineBookingsDateRange

try {
    $result = $apiInstance->apiV1InternalBookkeepingBookingImmutabilityReversedDaterangePatch($sDTracingId, $enshrineBookingsDateRange);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RulemasterApi->apiV1InternalBookkeepingBookingImmutabilityReversedDaterangePatch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **enshrineBookingsDateRange** | [**\OpenAPI\Client\Model\EnshrineBookingsDateRange**](../Model/EnshrineBookingsDateRange.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\EnshrineResponse**](../Model/EnshrineResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalBookkeepingBookingImmutabilityReversedDocumentsPatch()`

```php
apiV1InternalBookkeepingBookingImmutabilityReversedDocumentsPatch($sDTracingId, $enshrineDocuments): \OpenAPI\Client\Model\EnshrineResponse
```

Use this endpoint when the immutability of bookings of documents and payments have to be reversed.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RulemasterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$enshrineDocuments = new \OpenAPI\Client\Model\EnshrineDocuments(); // \OpenAPI\Client\Model\EnshrineDocuments

try {
    $result = $apiInstance->apiV1InternalBookkeepingBookingImmutabilityReversedDocumentsPatch($sDTracingId, $enshrineDocuments);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RulemasterApi->apiV1InternalBookkeepingBookingImmutabilityReversedDocumentsPatch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **enshrineDocuments** | [**\OpenAPI\Client\Model\EnshrineDocuments**](../Model/EnshrineDocuments.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\EnshrineResponse**](../Model/EnshrineResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalRebookingDocumentTypeDocumentIdGet()`

```php
apiV1InternalRebookingDocumentTypeDocumentIdGet($documentType, $documentId, $sDTracingId)
```

This endpoint is used to trigger a rebooking for a specific document.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RulemasterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$documentType = Invoice; // string
$documentId = 456; // float
$sDTracingId = 'sDTracingId_example'; // string

try {
    $apiInstance->apiV1InternalRebookingDocumentTypeDocumentIdGet($documentType, $documentId, $sDTracingId);
} catch (Exception $e) {
    echo 'Exception when calling RulemasterApi->apiV1InternalRebookingDocumentTypeDocumentIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **documentType** | **string**|  | |
| **documentId** | **float**|  | |
| **sDTracingId** | **string**|  | [optional] |

### Return type

void (empty response body)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `findAllBookingKeysBy()`

```php
findAllBookingKeysBy(): \OpenAPI\Client\Model\RuleMasterBookingKey[]
```

Use this endpoint to fetch all Rulemaster Booking Keys for display in View

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RulemasterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->findAllBookingKeysBy();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RulemasterApi->findAllBookingKeysBy: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\Model\RuleMasterBookingKey[]**](../Model/RuleMasterBookingKey.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
