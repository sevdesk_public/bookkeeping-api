# OpenAPI\Client\AccountingApi

All URIs are relative to http://localhost:8080, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/advanceinvoicepaid | Use this endpoint when a advance invoice has been paid and the corresponding bookings are to be generated for it. |
| [**apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/advanceinvoicepaid/validate | Use this end point to validate an advance invoice before triggering the booking process. |
| [**apiV1InternalAccountingBookingAccountingeventAdvanceinvoicesentValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventAdvanceinvoicesentValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/advanceinvoicesent/validate | Use this endpoint to validate an advance invoice before sending it out. |
| [**apiV1InternalAccountingBookingAccountingeventBanktransactionBookPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventBanktransactionBookPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/banktransaction/book | Use this endpoint to generate bookings for a bank transaction event. |
| [**apiV1InternalAccountingBookingAccountingeventBanktransactionReversePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventBanktransactionReversePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/banktransaction/reverse | Use this endpoint to reverse bookings for a bank transaction event. |
| [**apiV1InternalAccountingBookingAccountingeventBanktransactionValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventBanktransactionValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/banktransaction/validate | Use this endpoint to generate bookings for a bank transaction event. |
| [**apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/cancellationinvoicesent | Use this endpoint when a cancellation invoice has been sent and  the corresponding bookings are to be generated for it. |
| [**apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/cancellationinvoicesent/validate | Use this end point when a cancellation invoice has been sent and the corresponding bookings are to be generated. |
| [**apiV1InternalAccountingBookingAccountingeventCreditnotepaidPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCreditnotepaidPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnotepaid | Use this endpoint when a credit note has been paid and the corresponding bookings are to be created. |
| [**apiV1InternalAccountingBookingAccountingeventCreditnotepaidValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCreditnotepaidValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnotepaid/validate | Use this end point to validate a credit note before triggering the booking process. |
| [**apiV1InternalAccountingBookingAccountingeventCreditnotesentPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCreditnotesentPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnotesent | Use this endpoint when a credit note has been sent and the corresponding bookings are to be created. |
| [**apiV1InternalAccountingBookingAccountingeventCreditnotesentValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCreditnotesentValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnotesent/validate | Use this end point to validate a credit note before triggering the booking process. |
| [**apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnoteunderachievementpaid | Use this endpoint when a credit note underachievement has been paid and the corresponding bookings are to be created. |
| [**apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnoteunderachievementpaid/validate | Use this end point to validate a credit note underachievement before triggering the booking process. |
| [**apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnoteunderachievementsent | Use this endpoint when a credit note underachievement has been sent and the corresponding bookings are to be created. |
| [**apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnoteunderachievementsent/validate | Use this end point to validate a credit note underachievement before triggering the booking process. |
| [**apiV1InternalAccountingBookingAccountingeventDepreciationPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventDepreciationPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/depreciation | Use this endpoint to create bookings for depreciations. |
| [**apiV1InternalAccountingBookingAccountingeventDepreciationcorrectionPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventDepreciationcorrectionPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/depreciationcorrection | Use this endpoint to create correction bookings for depreciations. |
| [**apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/finalinvoicepaid | Use this end point to create the payment bookings for a final invoice. |
| [**apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/finalinvoicepaid/validate | Use this end point to validate a final invoice before triggering the booking process. |
| [**apiV1InternalAccountingBookingAccountingeventFinalinvoicesentPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventFinalinvoicesentPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/finalinvoicesent | Use this end point to create the sent bookings for a final invoice. |
| [**apiV1InternalAccountingBookingAccountingeventFinalinvoicesentValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventFinalinvoicesentValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/finalinvoicesent/validate | Use this end point to validate a final invoice before sending it out. |
| [**apiV1InternalAccountingBookingAccountingeventInvoicepaidPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventInvoicepaidPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/invoicepaid | Use this endpoint when an invoice has been paid and the corresponding bookings are to be created. |
| [**apiV1InternalAccountingBookingAccountingeventInvoicepaidValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventInvoicepaidValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/invoicepaid/validate | Use this end point to validate an invoice before triggering the booking process. |
| [**apiV1InternalAccountingBookingAccountingeventInvoicesentPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventInvoicesentPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/invoicesent | Use this endpoint when an invoice has been sent and the corresponding bookings are to be created. |
| [**apiV1InternalAccountingBookingAccountingeventInvoicesentValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventInvoicesentValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/invoicesent/validate | Use this endpoint to validate an invoice before sending it out. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptentertainmentpaid | Use this endpoint whenever a receipt with entertainment expenses has been paid  and the corresponding bookings are to be generated for it. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptentertainmentpaid/validate | Use this endpoint when validating an entertainment receipt before paying it. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptentertainmentregistered | Use this endpoint whenever a receipt with entertainment expenses has been registered  and the corresponding bookings are to be generated for it. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptentertainmentregistered/validate | Use this endpoint when validating an entertainment receipt before registering it. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptexpensepaid | Use this endpoint when an expense receipt has been paid  and the corresponding bookings are to be generated for it. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptexpensepaid/validate | Use this end point to validate a receipt expense before triggering the booking process. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptexpenseregistered | Use this endpoint when an expense receipt has been registered  and the corresponding bookings are to be generated for it. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptexpenseregistered/validate | Use this endpoint when validating an expense receipt before registering it. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptrevenuepaid | Use this endpoint when a receipt revenue has been paid and the corresponding bookings are to be created. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptrevenuepaid/validate | Use this endpoint when validating a revenue receipt before paying it. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredPost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredPost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptrevenueregistered | Use this endpoint when a receipt revenue has been registered and the corresponding bookings are to be created. |
| [**apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredValidatePost()**](AccountingApi.md#apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredValidatePost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptrevenueregistered/validate | Use this endpoint when validating a revenue receipt before registering it. |
| [**apiV1InternalAccountingBookingAllpaymentsdetachedDelete()**](AccountingApi.md#apiV1InternalAccountingBookingAllpaymentsdetachedDelete) | **DELETE** /api/v1/_internal/accounting/booking/allpaymentsdetached | Use this endpoint when a document has been set to status &#39;open&#39;. |
| [**apiV1InternalAccountingBookingCompanysettingsBydocumentidandtypeGet()**](AccountingApi.md#apiV1InternalAccountingBookingCompanysettingsBydocumentidandtypeGet) | **GET** /api/v1/_internal/accounting/booking/companysettings/bydocumentidandtype | Use this endpoint to get the company settings (taxation type and profit assessment type) used for the sent event booking of a given document. |
| [**apiV1InternalAccountingBookingCompanysettingsDocumentsGet()**](AccountingApi.md#apiV1InternalAccountingBookingCompanysettingsDocumentsGet) | **GET** /api/v1/_internal/accounting/booking/companysettings/documents | Use this endpoint to check if a change of company settings is possbile. |
| [**apiV1InternalAccountingBookingCompanysettingsRebookPost()**](AccountingApi.md#apiV1InternalAccountingBookingCompanysettingsRebookPost) | **POST** /api/v1/_internal/accounting/booking/companysettings/rebook | Use this endpoint to trigger the rebooking of relevant bookings when performing a company setting change. |
| [**apiV1InternalAccountingBookingDocumentDeletedDelete()**](AccountingApi.md#apiV1InternalAccountingBookingDocumentDeletedDelete) | **DELETE** /api/v1/_internal/accounting/booking/document/deleted | Use this endpoint when a document has been deleted and the sent event bookings have to be deleted. |
| [**apiV1InternalAccountingBookingDocumentEditedDelete()**](AccountingApi.md#apiV1InternalAccountingBookingDocumentEditedDelete) | **DELETE** /api/v1/_internal/accounting/booking/document/edited | Use this endpoint when a document has been edited and the sent event bookings have to be deleted. |
| [**apiV1InternalAccountingBookingPaymentdetachedDelete()**](AccountingApi.md#apiV1InternalAccountingBookingPaymentdetachedDelete) | **DELETE** /api/v1/_internal/accounting/booking/paymentdetached | Use this endpoint when a transaction has been detached from a document. |
| [**apiV1InternalAccountingBookingResetDelete()**](AccountingApi.md#apiV1InternalAccountingBookingResetDelete) | **DELETE** /api/v1/_internal/accounting/booking/reset | Use this endpoint to delete all booking data of a client. |


## `apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidPost()`

```php
apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidPost($sDTracingId, $advanceInvoicePaidEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a advance invoice has been paid and the corresponding bookings are to be generated for it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$advanceInvoicePaidEvent = new \OpenAPI\Client\Model\AdvanceInvoicePaidEvent(); // \OpenAPI\Client\Model\AdvanceInvoicePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidPost($sDTracingId, $advanceInvoicePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **advanceInvoicePaidEvent** | [**\OpenAPI\Client\Model\AdvanceInvoicePaidEvent**](../Model/AdvanceInvoicePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidValidatePost($sDTracingId, $advanceInvoicePaidEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point to validate an advance invoice before triggering the booking process.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$advanceInvoicePaidEvent = new \OpenAPI\Client\Model\AdvanceInvoicePaidEvent(); // \OpenAPI\Client\Model\AdvanceInvoicePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidValidatePost($sDTracingId, $advanceInvoicePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **advanceInvoicePaidEvent** | [**\OpenAPI\Client\Model\AdvanceInvoicePaidEvent**](../Model/AdvanceInvoicePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventAdvanceinvoicesentValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventAdvanceinvoicesentValidatePost($sDTracingId, $advanceInvoiceSentEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this endpoint to validate an advance invoice before sending it out.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$advanceInvoiceSentEvent = new \OpenAPI\Client\Model\AdvanceInvoiceSentEvent(); // \OpenAPI\Client\Model\AdvanceInvoiceSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventAdvanceinvoicesentValidatePost($sDTracingId, $advanceInvoiceSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventAdvanceinvoicesentValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **advanceInvoiceSentEvent** | [**\OpenAPI\Client\Model\AdvanceInvoiceSentEvent**](../Model/AdvanceInvoiceSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventBanktransactionBookPost()`

```php
apiV1InternalAccountingBookingAccountingeventBanktransactionBookPost($sDTracingId, $apiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint to generate bookings for a bank transaction event.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$apiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest = new \OpenAPI\Client\Model\ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest(); // \OpenAPI\Client\Model\ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventBanktransactionBookPost($sDTracingId, $apiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventBanktransactionBookPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **apiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest** | [**\OpenAPI\Client\Model\ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest**](../Model/ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventBanktransactionReversePost()`

```php
apiV1InternalAccountingBookingAccountingeventBanktransactionReversePost($sDTracingId, $bankTransactionReverseEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint to reverse bookings for a bank transaction event.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$bankTransactionReverseEvent = new \OpenAPI\Client\Model\BankTransactionReverseEvent(); // \OpenAPI\Client\Model\BankTransactionReverseEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventBanktransactionReversePost($sDTracingId, $bankTransactionReverseEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventBanktransactionReversePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **bankTransactionReverseEvent** | [**\OpenAPI\Client\Model\BankTransactionReverseEvent**](../Model/BankTransactionReverseEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventBanktransactionValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventBanktransactionValidatePost($sDTracingId, $apiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this endpoint to generate bookings for a bank transaction event.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$apiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest = new \OpenAPI\Client\Model\ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest(); // \OpenAPI\Client\Model\ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventBanktransactionValidatePost($sDTracingId, $apiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventBanktransactionValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **apiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest** | [**\OpenAPI\Client\Model\ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest**](../Model/ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentPost()`

```php
apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentPost($sDTracingId, $cancellationInvoiceSentEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a cancellation invoice has been sent and  the corresponding bookings are to be generated for it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$cancellationInvoiceSentEvent = new \OpenAPI\Client\Model\CancellationInvoiceSentEvent(); // \OpenAPI\Client\Model\CancellationInvoiceSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentPost($sDTracingId, $cancellationInvoiceSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **cancellationInvoiceSentEvent** | [**\OpenAPI\Client\Model\CancellationInvoiceSentEvent**](../Model/CancellationInvoiceSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentValidatePost($sDTracingId, $cancellationInvoiceSentEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point when a cancellation invoice has been sent and the corresponding bookings are to be generated.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$cancellationInvoiceSentEvent = new \OpenAPI\Client\Model\CancellationInvoiceSentEvent(); // \OpenAPI\Client\Model\CancellationInvoiceSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentValidatePost($sDTracingId, $cancellationInvoiceSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **cancellationInvoiceSentEvent** | [**\OpenAPI\Client\Model\CancellationInvoiceSentEvent**](../Model/CancellationInvoiceSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCreditnotepaidPost()`

```php
apiV1InternalAccountingBookingAccountingeventCreditnotepaidPost($sDTracingId, $creditNotePaidEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a credit note has been paid and the corresponding bookings are to be created.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$creditNotePaidEvent = new \OpenAPI\Client\Model\CreditNotePaidEvent(); // \OpenAPI\Client\Model\CreditNotePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCreditnotepaidPost($sDTracingId, $creditNotePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCreditnotepaidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **creditNotePaidEvent** | [**\OpenAPI\Client\Model\CreditNotePaidEvent**](../Model/CreditNotePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCreditnotepaidValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventCreditnotepaidValidatePost($sDTracingId, $creditNotePaidEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point to validate a credit note before triggering the booking process.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$creditNotePaidEvent = new \OpenAPI\Client\Model\CreditNotePaidEvent(); // \OpenAPI\Client\Model\CreditNotePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCreditnotepaidValidatePost($sDTracingId, $creditNotePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCreditnotepaidValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **creditNotePaidEvent** | [**\OpenAPI\Client\Model\CreditNotePaidEvent**](../Model/CreditNotePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCreditnotesentPost()`

```php
apiV1InternalAccountingBookingAccountingeventCreditnotesentPost($sDTracingId, $creditNoteSentEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a credit note has been sent and the corresponding bookings are to be created.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$creditNoteSentEvent = new \OpenAPI\Client\Model\CreditNoteSentEvent(); // \OpenAPI\Client\Model\CreditNoteSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCreditnotesentPost($sDTracingId, $creditNoteSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCreditnotesentPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **creditNoteSentEvent** | [**\OpenAPI\Client\Model\CreditNoteSentEvent**](../Model/CreditNoteSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCreditnotesentValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventCreditnotesentValidatePost($sDTracingId, $creditNoteSentEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point to validate a credit note before triggering the booking process.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$creditNoteSentEvent = new \OpenAPI\Client\Model\CreditNoteSentEvent(); // \OpenAPI\Client\Model\CreditNoteSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCreditnotesentValidatePost($sDTracingId, $creditNoteSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCreditnotesentValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **creditNoteSentEvent** | [**\OpenAPI\Client\Model\CreditNoteSentEvent**](../Model/CreditNoteSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidPost()`

```php
apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidPost($sDTracingId, $creditNoteUnderachievementPaidEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a credit note underachievement has been paid and the corresponding bookings are to be created.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$creditNoteUnderachievementPaidEvent = new \OpenAPI\Client\Model\CreditNoteUnderachievementPaidEvent(); // \OpenAPI\Client\Model\CreditNoteUnderachievementPaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidPost($sDTracingId, $creditNoteUnderachievementPaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **creditNoteUnderachievementPaidEvent** | [**\OpenAPI\Client\Model\CreditNoteUnderachievementPaidEvent**](../Model/CreditNoteUnderachievementPaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidValidatePost($sDTracingId, $creditNoteUnderachievementPaidEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point to validate a credit note underachievement before triggering the booking process.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$creditNoteUnderachievementPaidEvent = new \OpenAPI\Client\Model\CreditNoteUnderachievementPaidEvent(); // \OpenAPI\Client\Model\CreditNoteUnderachievementPaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidValidatePost($sDTracingId, $creditNoteUnderachievementPaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **creditNoteUnderachievementPaidEvent** | [**\OpenAPI\Client\Model\CreditNoteUnderachievementPaidEvent**](../Model/CreditNoteUnderachievementPaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentPost()`

```php
apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentPost($sDTracingId, $creditNoteUnderachievementSentEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a credit note underachievement has been sent and the corresponding bookings are to be created.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$creditNoteUnderachievementSentEvent = new \OpenAPI\Client\Model\CreditNoteUnderachievementSentEvent(); // \OpenAPI\Client\Model\CreditNoteUnderachievementSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentPost($sDTracingId, $creditNoteUnderachievementSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **creditNoteUnderachievementSentEvent** | [**\OpenAPI\Client\Model\CreditNoteUnderachievementSentEvent**](../Model/CreditNoteUnderachievementSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentValidatePost($sDTracingId, $creditNoteUnderachievementSentEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point to validate a credit note underachievement before triggering the booking process.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$creditNoteUnderachievementSentEvent = new \OpenAPI\Client\Model\CreditNoteUnderachievementSentEvent(); // \OpenAPI\Client\Model\CreditNoteUnderachievementSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentValidatePost($sDTracingId, $creditNoteUnderachievementSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **creditNoteUnderachievementSentEvent** | [**\OpenAPI\Client\Model\CreditNoteUnderachievementSentEvent**](../Model/CreditNoteUnderachievementSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventDepreciationPost()`

```php
apiV1InternalAccountingBookingAccountingeventDepreciationPost($sDTracingId, $depreciationEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint to create bookings for depreciations.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$depreciationEvent = new \OpenAPI\Client\Model\DepreciationEvent(); // \OpenAPI\Client\Model\DepreciationEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventDepreciationPost($sDTracingId, $depreciationEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventDepreciationPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **depreciationEvent** | [**\OpenAPI\Client\Model\DepreciationEvent**](../Model/DepreciationEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventDepreciationcorrectionPost()`

```php
apiV1InternalAccountingBookingAccountingeventDepreciationcorrectionPost($sDTracingId, $depreciationCorrectionEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint to create correction bookings for depreciations.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$depreciationCorrectionEvent = new \OpenAPI\Client\Model\DepreciationCorrectionEvent(); // \OpenAPI\Client\Model\DepreciationCorrectionEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventDepreciationcorrectionPost($sDTracingId, $depreciationCorrectionEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventDepreciationcorrectionPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **depreciationCorrectionEvent** | [**\OpenAPI\Client\Model\DepreciationCorrectionEvent**](../Model/DepreciationCorrectionEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidPost()`

```php
apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidPost($sDTracingId, $finalInvoicePaidEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this end point to create the payment bookings for a final invoice.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$finalInvoicePaidEvent = new \OpenAPI\Client\Model\FinalInvoicePaidEvent(); // \OpenAPI\Client\Model\FinalInvoicePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidPost($sDTracingId, $finalInvoicePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **finalInvoicePaidEvent** | [**\OpenAPI\Client\Model\FinalInvoicePaidEvent**](../Model/FinalInvoicePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidValidatePost($sDTracingId, $finalInvoicePaidEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point to validate a final invoice before triggering the booking process.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$finalInvoicePaidEvent = new \OpenAPI\Client\Model\FinalInvoicePaidEvent(); // \OpenAPI\Client\Model\FinalInvoicePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidValidatePost($sDTracingId, $finalInvoicePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **finalInvoicePaidEvent** | [**\OpenAPI\Client\Model\FinalInvoicePaidEvent**](../Model/FinalInvoicePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventFinalinvoicesentPost()`

```php
apiV1InternalAccountingBookingAccountingeventFinalinvoicesentPost($sDTracingId, $finalInvoiceSentEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this end point to create the sent bookings for a final invoice.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$finalInvoiceSentEvent = new \OpenAPI\Client\Model\FinalInvoiceSentEvent(); // \OpenAPI\Client\Model\FinalInvoiceSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventFinalinvoicesentPost($sDTracingId, $finalInvoiceSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventFinalinvoicesentPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **finalInvoiceSentEvent** | [**\OpenAPI\Client\Model\FinalInvoiceSentEvent**](../Model/FinalInvoiceSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventFinalinvoicesentValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventFinalinvoicesentValidatePost($sDTracingId, $finalInvoiceSentEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point to validate a final invoice before sending it out.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$finalInvoiceSentEvent = new \OpenAPI\Client\Model\FinalInvoiceSentEvent(); // \OpenAPI\Client\Model\FinalInvoiceSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventFinalinvoicesentValidatePost($sDTracingId, $finalInvoiceSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventFinalinvoicesentValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **finalInvoiceSentEvent** | [**\OpenAPI\Client\Model\FinalInvoiceSentEvent**](../Model/FinalInvoiceSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventInvoicepaidPost()`

```php
apiV1InternalAccountingBookingAccountingeventInvoicepaidPost($sDTracingId, $invoicePaidEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when an invoice has been paid and the corresponding bookings are to be created.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$invoicePaidEvent = new \OpenAPI\Client\Model\InvoicePaidEvent(); // \OpenAPI\Client\Model\InvoicePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventInvoicepaidPost($sDTracingId, $invoicePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventInvoicepaidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **invoicePaidEvent** | [**\OpenAPI\Client\Model\InvoicePaidEvent**](../Model/InvoicePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventInvoicepaidValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventInvoicepaidValidatePost($sDTracingId, $invoicePaidEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point to validate an invoice before triggering the booking process.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$invoicePaidEvent = new \OpenAPI\Client\Model\InvoicePaidEvent(); // \OpenAPI\Client\Model\InvoicePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventInvoicepaidValidatePost($sDTracingId, $invoicePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventInvoicepaidValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **invoicePaidEvent** | [**\OpenAPI\Client\Model\InvoicePaidEvent**](../Model/InvoicePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventInvoicesentPost()`

```php
apiV1InternalAccountingBookingAccountingeventInvoicesentPost($sDTracingId, $invoiceSentEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when an invoice has been sent and the corresponding bookings are to be created.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$invoiceSentEvent = new \OpenAPI\Client\Model\InvoiceSentEvent(); // \OpenAPI\Client\Model\InvoiceSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventInvoicesentPost($sDTracingId, $invoiceSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventInvoicesentPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **invoiceSentEvent** | [**\OpenAPI\Client\Model\InvoiceSentEvent**](../Model/InvoiceSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventInvoicesentValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventInvoicesentValidatePost($sDTracingId, $invoiceSentEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this endpoint to validate an invoice before sending it out.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$invoiceSentEvent = new \OpenAPI\Client\Model\InvoiceSentEvent(); // \OpenAPI\Client\Model\InvoiceSentEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventInvoicesentValidatePost($sDTracingId, $invoiceSentEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventInvoicesentValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **invoiceSentEvent** | [**\OpenAPI\Client\Model\InvoiceSentEvent**](../Model/InvoiceSentEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidPost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidPost($sDTracingId, $receiptEntertainmentPaidEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint whenever a receipt with entertainment expenses has been paid  and the corresponding bookings are to be generated for it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptEntertainmentPaidEvent = new \OpenAPI\Client\Model\ReceiptEntertainmentPaidEvent(); // \OpenAPI\Client\Model\ReceiptEntertainmentPaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidPost($sDTracingId, $receiptEntertainmentPaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptEntertainmentPaidEvent** | [**\OpenAPI\Client\Model\ReceiptEntertainmentPaidEvent**](../Model/ReceiptEntertainmentPaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidValidatePost($sDTracingId, $receiptEntertainmentPaidEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this endpoint when validating an entertainment receipt before paying it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptEntertainmentPaidEvent = new \OpenAPI\Client\Model\ReceiptEntertainmentPaidEvent(); // \OpenAPI\Client\Model\ReceiptEntertainmentPaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidValidatePost($sDTracingId, $receiptEntertainmentPaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptEntertainmentPaidEvent** | [**\OpenAPI\Client\Model\ReceiptEntertainmentPaidEvent**](../Model/ReceiptEntertainmentPaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredPost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredPost($sDTracingId, $receiptEntertainmentRegisteredEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint whenever a receipt with entertainment expenses has been registered  and the corresponding bookings are to be generated for it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptEntertainmentRegisteredEvent = new \OpenAPI\Client\Model\ReceiptEntertainmentRegisteredEvent(); // \OpenAPI\Client\Model\ReceiptEntertainmentRegisteredEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredPost($sDTracingId, $receiptEntertainmentRegisteredEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptEntertainmentRegisteredEvent** | [**\OpenAPI\Client\Model\ReceiptEntertainmentRegisteredEvent**](../Model/ReceiptEntertainmentRegisteredEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredValidatePost($sDTracingId, $receiptEntertainmentRegisteredEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this endpoint when validating an entertainment receipt before registering it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptEntertainmentRegisteredEvent = new \OpenAPI\Client\Model\ReceiptEntertainmentRegisteredEvent(); // \OpenAPI\Client\Model\ReceiptEntertainmentRegisteredEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredValidatePost($sDTracingId, $receiptEntertainmentRegisteredEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptEntertainmentRegisteredEvent** | [**\OpenAPI\Client\Model\ReceiptEntertainmentRegisteredEvent**](../Model/ReceiptEntertainmentRegisteredEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidPost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidPost($sDTracingId, $receiptExpensePaidEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when an expense receipt has been paid  and the corresponding bookings are to be generated for it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptExpensePaidEvent = new \OpenAPI\Client\Model\ReceiptExpensePaidEvent(); // \OpenAPI\Client\Model\ReceiptExpensePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidPost($sDTracingId, $receiptExpensePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptExpensePaidEvent** | [**\OpenAPI\Client\Model\ReceiptExpensePaidEvent**](../Model/ReceiptExpensePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidValidatePost($sDTracingId, $receiptExpensePaidEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this end point to validate a receipt expense before triggering the booking process.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptExpensePaidEvent = new \OpenAPI\Client\Model\ReceiptExpensePaidEvent(); // \OpenAPI\Client\Model\ReceiptExpensePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidValidatePost($sDTracingId, $receiptExpensePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptExpensePaidEvent** | [**\OpenAPI\Client\Model\ReceiptExpensePaidEvent**](../Model/ReceiptExpensePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredPost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredPost($sDTracingId, $receiptExpenseRegisteredEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when an expense receipt has been registered  and the corresponding bookings are to be generated for it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptExpenseRegisteredEvent = new \OpenAPI\Client\Model\ReceiptExpenseRegisteredEvent(); // \OpenAPI\Client\Model\ReceiptExpenseRegisteredEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredPost($sDTracingId, $receiptExpenseRegisteredEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptExpenseRegisteredEvent** | [**\OpenAPI\Client\Model\ReceiptExpenseRegisteredEvent**](../Model/ReceiptExpenseRegisteredEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredValidatePost($sDTracingId, $receiptExpenseRegisteredEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this endpoint when validating an expense receipt before registering it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptExpenseRegisteredEvent = new \OpenAPI\Client\Model\ReceiptExpenseRegisteredEvent(); // \OpenAPI\Client\Model\ReceiptExpenseRegisteredEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredValidatePost($sDTracingId, $receiptExpenseRegisteredEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptExpenseRegisteredEvent** | [**\OpenAPI\Client\Model\ReceiptExpenseRegisteredEvent**](../Model/ReceiptExpenseRegisteredEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidPost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidPost($sDTracingId, $receiptRevenuePaidEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a receipt revenue has been paid and the corresponding bookings are to be created.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptRevenuePaidEvent = new \OpenAPI\Client\Model\ReceiptRevenuePaidEvent(); // \OpenAPI\Client\Model\ReceiptRevenuePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidPost($sDTracingId, $receiptRevenuePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptRevenuePaidEvent** | [**\OpenAPI\Client\Model\ReceiptRevenuePaidEvent**](../Model/ReceiptRevenuePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidValidatePost($sDTracingId, $receiptRevenuePaidEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this endpoint when validating a revenue receipt before paying it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptRevenuePaidEvent = new \OpenAPI\Client\Model\ReceiptRevenuePaidEvent(); // \OpenAPI\Client\Model\ReceiptRevenuePaidEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidValidatePost($sDTracingId, $receiptRevenuePaidEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptRevenuePaidEvent** | [**\OpenAPI\Client\Model\ReceiptRevenuePaidEvent**](../Model/ReceiptRevenuePaidEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredPost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredPost($sDTracingId, $receiptRevenueRegisteredEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a receipt revenue has been registered and the corresponding bookings are to be created.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptRevenueRegisteredEvent = new \OpenAPI\Client\Model\ReceiptRevenueRegisteredEvent(); // \OpenAPI\Client\Model\ReceiptRevenueRegisteredEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredPost($sDTracingId, $receiptRevenueRegisteredEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptRevenueRegisteredEvent** | [**\OpenAPI\Client\Model\ReceiptRevenueRegisteredEvent**](../Model/ReceiptRevenueRegisteredEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredValidatePost()`

```php
apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredValidatePost($sDTracingId, $receiptRevenueRegisteredEvent): \OpenAPI\Client\Model\AccountingEventValidationResponse
```

Use this endpoint when validating a revenue receipt before registering it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$receiptRevenueRegisteredEvent = new \OpenAPI\Client\Model\ReceiptRevenueRegisteredEvent(); // \OpenAPI\Client\Model\ReceiptRevenueRegisteredEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredValidatePost($sDTracingId, $receiptRevenueRegisteredEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredValidatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **receiptRevenueRegisteredEvent** | [**\OpenAPI\Client\Model\ReceiptRevenueRegisteredEvent**](../Model/ReceiptRevenueRegisteredEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventValidationResponse**](../Model/AccountingEventValidationResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingAllpaymentsdetachedDelete()`

```php
apiV1InternalAccountingBookingAllpaymentsdetachedDelete($sDTracingId, $allPaymentsDetachedEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a document has been set to status 'open'.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$allPaymentsDetachedEvent = new \OpenAPI\Client\Model\AllPaymentsDetachedEvent(); // \OpenAPI\Client\Model\AllPaymentsDetachedEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingAllpaymentsdetachedDelete($sDTracingId, $allPaymentsDetachedEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingAllpaymentsdetachedDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **allPaymentsDetachedEvent** | [**\OpenAPI\Client\Model\AllPaymentsDetachedEvent**](../Model/AllPaymentsDetachedEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingCompanysettingsBydocumentidandtypeGet()`

```php
apiV1InternalAccountingBookingCompanysettingsBydocumentidandtypeGet($documentId, $documentType, $sDTracingId): \OpenAPI\Client\Model\CompanySettingsEventResponse
```

Use this endpoint to get the company settings (taxation type and profit assessment type) used for the sent event booking of a given document.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$documentId = 1337; // string
$documentType = Invoice; // string
$sDTracingId = 'sDTracingId_example'; // string

try {
    $result = $apiInstance->apiV1InternalAccountingBookingCompanysettingsBydocumentidandtypeGet($documentId, $documentType, $sDTracingId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingCompanysettingsBydocumentidandtypeGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **documentId** | **string**|  | |
| **documentType** | **string**|  | |
| **sDTracingId** | **string**|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\CompanySettingsEventResponse**](../Model/CompanySettingsEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingCompanysettingsDocumentsGet()`

```php
apiV1InternalAccountingBookingCompanysettingsDocumentsGet(): \OpenAPI\Client\Model\DocumentBookingResponse
```

Use this endpoint to check if a change of company settings is possbile.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->apiV1InternalAccountingBookingCompanysettingsDocumentsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingCompanysettingsDocumentsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\Model\DocumentBookingResponse**](../Model/DocumentBookingResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingCompanysettingsRebookPost()`

```php
apiV1InternalAccountingBookingCompanysettingsRebookPost($sDTracingId, $companySettingRebookingParameters)
```

Use this endpoint to trigger the rebooking of relevant bookings when performing a company setting change.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$companySettingRebookingParameters = new \OpenAPI\Client\Model\CompanySettingRebookingParameters(); // \OpenAPI\Client\Model\CompanySettingRebookingParameters

try {
    $apiInstance->apiV1InternalAccountingBookingCompanysettingsRebookPost($sDTracingId, $companySettingRebookingParameters);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingCompanysettingsRebookPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **companySettingRebookingParameters** | [**\OpenAPI\Client\Model\CompanySettingRebookingParameters**](../Model/CompanySettingRebookingParameters.md)|  | [optional] |

### Return type

void (empty response body)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingDocumentDeletedDelete()`

```php
apiV1InternalAccountingBookingDocumentDeletedDelete($sDTracingId, $documentDeletedEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a document has been deleted and the sent event bookings have to be deleted.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$documentDeletedEvent = new \OpenAPI\Client\Model\DocumentDeletedEvent(); // \OpenAPI\Client\Model\DocumentDeletedEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingDocumentDeletedDelete($sDTracingId, $documentDeletedEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingDocumentDeletedDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **documentDeletedEvent** | [**\OpenAPI\Client\Model\DocumentDeletedEvent**](../Model/DocumentDeletedEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingDocumentEditedDelete()`

```php
apiV1InternalAccountingBookingDocumentEditedDelete($sDTracingId, $documentEditedEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a document has been edited and the sent event bookings have to be deleted.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$documentEditedEvent = new \OpenAPI\Client\Model\DocumentEditedEvent(); // \OpenAPI\Client\Model\DocumentEditedEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingDocumentEditedDelete($sDTracingId, $documentEditedEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingDocumentEditedDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **documentEditedEvent** | [**\OpenAPI\Client\Model\DocumentEditedEvent**](../Model/DocumentEditedEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingPaymentdetachedDelete()`

```php
apiV1InternalAccountingBookingPaymentdetachedDelete($sDTracingId, $paymentDetachedEvent): \OpenAPI\Client\Model\AccountingEventResponse
```

Use this endpoint when a transaction has been detached from a document.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$paymentDetachedEvent = new \OpenAPI\Client\Model\PaymentDetachedEvent(); // \OpenAPI\Client\Model\PaymentDetachedEvent

try {
    $result = $apiInstance->apiV1InternalAccountingBookingPaymentdetachedDelete($sDTracingId, $paymentDetachedEvent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingPaymentdetachedDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **paymentDetachedEvent** | [**\OpenAPI\Client\Model\PaymentDetachedEvent**](../Model/PaymentDetachedEvent.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountingEventResponse**](../Model/AccountingEventResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingBookingResetDelete()`

```php
apiV1InternalAccountingBookingResetDelete($sDTracingId): \OpenAPI\Client\Model\ResetResultDto
```

Use this endpoint to delete all booking data of a client.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string

try {
    $result = $apiInstance->apiV1InternalAccountingBookingResetDelete($sDTracingId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountingApi->apiV1InternalAccountingBookingResetDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\ResetResultDto**](../Model/ResetResultDto.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
