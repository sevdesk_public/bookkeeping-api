# OpenAPI\Client\ReportApi

All URIs are relative to http://localhost:8080, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiV1InternalAccountingReportUstvaGet()**](ReportApi.md#apiV1InternalAccountingReportUstvaGet) | **GET** /api/v1/_internal/accounting/report/ustva | Returns the UStVA report for given period. |
| [**apiV1InternalAccountingReportUstvaKzfieldsGet()**](ReportApi.md#apiV1InternalAccountingReportUstvaKzfieldsGet) | **GET** /api/v1/_internal/accounting/report/ustva/kzfields | Returns kz field numbers grouped by the ustva row. |
| [**apiV1InternalReportBwaExportGet()**](ReportApi.md#apiV1InternalReportBwaExportGet) | **GET** /api/v1/_internal/report/bwa/export | Returns the BWA as PDF for the given period. |
| [**apiV1InternalReportBwaExportPost()**](ReportApi.md#apiV1InternalReportBwaExportPost) | **POST** /api/v1/_internal/report/bwa/export | Returns the BWA as PDF for the given period, filtered by documents. |
| [**apiV1InternalReportBwaGet()**](ReportApi.md#apiV1InternalReportBwaGet) | **GET** /api/v1/_internal/report/bwa | Returns the BWA report for the given period. |
| [**apiV1InternalReportBwaPost()**](ReportApi.md#apiV1InternalReportBwaPost) | **POST** /api/v1/_internal/report/bwa | Returns the BWA report for the given period, filtered by documents. |
| [**apiV1InternalReportComparativebwaGet()**](ReportApi.md#apiV1InternalReportComparativebwaGet) | **GET** /api/v1/_internal/report/comparativebwa | Returns the comparative BWA report for the given period. |
| [**apiV1InternalReportComparativebwaPost()**](ReportApi.md#apiV1InternalReportComparativebwaPost) | **POST** /api/v1/_internal/report/comparativebwa | Returns the comparative BWA report for the given period, filtered by documents. |


## `apiV1InternalAccountingReportUstvaGet()`

```php
apiV1InternalAccountingReportUstvaGet($startMonth, $endMonth): string
```

Returns the UStVA report for given period.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$startMonth = 2022-07; // string
$endMonth = 'endMonth_example'; // string

try {
    $result = $apiInstance->apiV1InternalAccountingReportUstvaGet($startMonth, $endMonth);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->apiV1InternalAccountingReportUstvaGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **startMonth** | **string**|  | |
| **endMonth** | **string**|  | [optional] |

### Return type

**string**

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingReportUstvaKzfieldsGet()`

```php
apiV1InternalAccountingReportUstvaKzfieldsGet(): \OpenAPI\Client\Model\KzFieldResponse
```

Returns kz field numbers grouped by the ustva row.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->apiV1InternalAccountingReportUstvaKzfieldsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->apiV1InternalAccountingReportUstvaKzfieldsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\Model\KzFieldResponse**](../Model/KzFieldResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalReportBwaExportGet()`

```php
apiV1InternalReportBwaExportGet($month, $companyName, $accountingProfitDeterminationType, $referenceMonth, $executiveDirectorName): \SplFileObject
```

Returns the BWA as PDF for the given period.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$month = 2022-10; // string
$companyName = 'companyName_example'; // string
$accountingProfitDeterminationType = new \OpenAPI\Client\Model\AccountingProfitDeterminationType(); // AccountingProfitDeterminationType
$referenceMonth = 2022-07; // string
$executiveDirectorName = 'executiveDirectorName_example'; // string

try {
    $result = $apiInstance->apiV1InternalReportBwaExportGet($month, $companyName, $accountingProfitDeterminationType, $referenceMonth, $executiveDirectorName);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->apiV1InternalReportBwaExportGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **month** | **string**|  | |
| **companyName** | **string**|  | |
| **accountingProfitDeterminationType** | [**AccountingProfitDeterminationType**](../Model/.md)|  | |
| **referenceMonth** | **string**|  | [optional] |
| **executiveDirectorName** | **string**|  | [optional] |

### Return type

**\SplFileObject**

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalReportBwaExportPost()`

```php
apiV1InternalReportBwaExportPost($month, $companyName, $accountingProfitDeterminationType, $referenceMonth, $executiveDirectorName, $bwaDocumentRef): \OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response1
```

Returns the BWA as PDF for the given period, filtered by documents.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$month = 2022-10; // string
$companyName = 'companyName_example'; // string
$accountingProfitDeterminationType = new \OpenAPI\Client\Model\AccountingProfitDeterminationType(); // AccountingProfitDeterminationType
$referenceMonth = 2022-07; // string
$executiveDirectorName = 'executiveDirectorName_example'; // string
$bwaDocumentRef = array(new \OpenAPI\Client\Model\BwaDocumentRef()); // \OpenAPI\Client\Model\BwaDocumentRef[]

try {
    $result = $apiInstance->apiV1InternalReportBwaExportPost($month, $companyName, $accountingProfitDeterminationType, $referenceMonth, $executiveDirectorName, $bwaDocumentRef);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->apiV1InternalReportBwaExportPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **month** | **string**|  | |
| **companyName** | **string**|  | |
| **accountingProfitDeterminationType** | [**AccountingProfitDeterminationType**](../Model/.md)|  | |
| **referenceMonth** | **string**|  | [optional] |
| **executiveDirectorName** | **string**|  | [optional] |
| **bwaDocumentRef** | [**\OpenAPI\Client\Model\BwaDocumentRef[]**](../Model/BwaDocumentRef.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response1**](../Model/ApiV1InternalReportComparativebwaGet200Response1.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalReportBwaGet()`

```php
apiV1InternalReportBwaGet($from, $until, $accountingProfitDeterminationType): \OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response1
```

Returns the BWA report for the given period.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$from = 2022-07; // string
$until = 2022-09; // string
$accountingProfitDeterminationType = new \OpenAPI\Client\Model\AccountingProfitDeterminationType(); // AccountingProfitDeterminationType

try {
    $result = $apiInstance->apiV1InternalReportBwaGet($from, $until, $accountingProfitDeterminationType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->apiV1InternalReportBwaGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **from** | **string**|  | |
| **until** | **string**|  | |
| **accountingProfitDeterminationType** | [**AccountingProfitDeterminationType**](../Model/.md)|  | |

### Return type

[**\OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response1**](../Model/ApiV1InternalReportComparativebwaGet200Response1.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalReportBwaPost()`

```php
apiV1InternalReportBwaPost($from, $until, $accountingProfitDeterminationType, $bwaDocumentRef): \OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response1
```

Returns the BWA report for the given period, filtered by documents.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$from = 2022-07; // string
$until = 2022-09; // string
$accountingProfitDeterminationType = new \OpenAPI\Client\Model\AccountingProfitDeterminationType(); // AccountingProfitDeterminationType
$bwaDocumentRef = array(new \OpenAPI\Client\Model\BwaDocumentRef()); // \OpenAPI\Client\Model\BwaDocumentRef[]

try {
    $result = $apiInstance->apiV1InternalReportBwaPost($from, $until, $accountingProfitDeterminationType, $bwaDocumentRef);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->apiV1InternalReportBwaPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **from** | **string**|  | |
| **until** | **string**|  | |
| **accountingProfitDeterminationType** | [**AccountingProfitDeterminationType**](../Model/.md)|  | |
| **bwaDocumentRef** | [**\OpenAPI\Client\Model\BwaDocumentRef[]**](../Model/BwaDocumentRef.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response1**](../Model/ApiV1InternalReportComparativebwaGet200Response1.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalReportComparativebwaGet()`

```php
apiV1InternalReportComparativebwaGet($accountingProfitDeterminationType, $month, $referenceMonth): \OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response
```

Returns the comparative BWA report for the given period.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accountingProfitDeterminationType = new \OpenAPI\Client\Model\AccountingProfitDeterminationType(); // AccountingProfitDeterminationType
$month = 2022-10; // string
$referenceMonth = 2022-07; // string

try {
    $result = $apiInstance->apiV1InternalReportComparativebwaGet($accountingProfitDeterminationType, $month, $referenceMonth);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->apiV1InternalReportComparativebwaGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **accountingProfitDeterminationType** | [**AccountingProfitDeterminationType**](../Model/.md)|  | |
| **month** | **string**|  | |
| **referenceMonth** | **string**|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response**](../Model/ApiV1InternalReportComparativebwaGet200Response.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalReportComparativebwaPost()`

```php
apiV1InternalReportComparativebwaPost($accountingProfitDeterminationType, $month, $referenceMonth, $bwaDocumentRef): \OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response1
```

Returns the comparative BWA report for the given period, filtered by documents.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\ReportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accountingProfitDeterminationType = new \OpenAPI\Client\Model\AccountingProfitDeterminationType(); // AccountingProfitDeterminationType
$month = 2022-10; // string
$referenceMonth = 2022-07; // string
$bwaDocumentRef = array(new \OpenAPI\Client\Model\BwaDocumentRef()); // \OpenAPI\Client\Model\BwaDocumentRef[]

try {
    $result = $apiInstance->apiV1InternalReportComparativebwaPost($accountingProfitDeterminationType, $month, $referenceMonth, $bwaDocumentRef);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReportApi->apiV1InternalReportComparativebwaPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **accountingProfitDeterminationType** | [**AccountingProfitDeterminationType**](../Model/.md)|  | |
| **month** | **string**|  | |
| **referenceMonth** | **string**|  | [optional] |
| **bwaDocumentRef** | [**\OpenAPI\Client\Model\BwaDocumentRef[]**](../Model/BwaDocumentRef.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\ApiV1InternalReportComparativebwaGet200Response1**](../Model/ApiV1InternalReportComparativebwaGet200Response1.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
