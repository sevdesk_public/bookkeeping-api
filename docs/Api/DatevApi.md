# OpenAPI\Client\DatevApi

All URIs are relative to http://localhost:8080, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiV1InternalAccountingDatevBookingEurBydocumentreferencesPost()**](DatevApi.md#apiV1InternalAccountingDatevBookingEurBydocumentreferencesPost) | **POST** /api/v1/_internal/accounting/datev/booking/eur/bydocumentreferences | Returns all bookings and accounting records grouped by document reference with the relevant information which is used for the export to DATEV for a client in the given time range. |
| [**apiV1InternalAccountingDatevBookingEurBypaymentGet()**](DatevApi.md#apiV1InternalAccountingDatevBookingEurBypaymentGet) | **GET** /api/v1/_internal/accounting/datev/booking/eur/bypayment | Returns all payment bookings and accounting records with the relevant information which is used for the export to DATEV for a client in the given time range. |
| [**apiV1InternalAccountingDatevBookingGet()**](DatevApi.md#apiV1InternalAccountingDatevBookingGet) | **GET** /api/v1/_internal/accounting/datev/booking | Returns all payment bookings and accounting records with the relevant information which is used for the export to DATEV for a client in the given time range. Revenue bookings are loaded using document date, payment bookings are loaded using the pay date. |


## `apiV1InternalAccountingDatevBookingEurBydocumentreferencesPost()`

```php
apiV1InternalAccountingDatevBookingEurBydocumentreferencesPost($sDTracingId, $offset, $limit, $documentRef): \OpenAPI\Client\Model\DatevBookingsResponse
```

Returns all bookings and accounting records grouped by document reference with the relevant information which is used for the export to DATEV for a client in the given time range.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$offset = 1000; // float
$limit = 10; // float
$documentRef = array(new \OpenAPI\Client\Model\DocumentRef()); // \OpenAPI\Client\Model\DocumentRef[]

try {
    $result = $apiInstance->apiV1InternalAccountingDatevBookingEurBydocumentreferencesPost($sDTracingId, $offset, $limit, $documentRef);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DatevApi->apiV1InternalAccountingDatevBookingEurBydocumentreferencesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **offset** | **float**|  | [optional] [default to 0] |
| **limit** | **float**|  | [optional] [default to 1000] |
| **documentRef** | [**\OpenAPI\Client\Model\DocumentRef[]**](../Model/DocumentRef.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\DatevBookingsResponse**](../Model/DatevBookingsResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingDatevBookingEurBypaymentGet()`

```php
apiV1InternalAccountingDatevBookingEurBypaymentGet($startDate, $endDate, $withEnshrined, $sDTracingId, $offset, $limit): \OpenAPI\Client\Model\DatevBookingsResponse
```

Returns all payment bookings and accounting records with the relevant information which is used for the export to DATEV for a client in the given time range.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$startDate = 2022-03-01; // string
$endDate = 2022-04-01; // string
$withEnshrined = true; // bool
$sDTracingId = 'sDTracingId_example'; // string
$offset = 1000; // float
$limit = 10; // float

try {
    $result = $apiInstance->apiV1InternalAccountingDatevBookingEurBypaymentGet($startDate, $endDate, $withEnshrined, $sDTracingId, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DatevApi->apiV1InternalAccountingDatevBookingEurBypaymentGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **startDate** | **string**|  | |
| **endDate** | **string**|  | |
| **withEnshrined** | **bool**|  | [default to false] |
| **sDTracingId** | **string**|  | [optional] |
| **offset** | **float**|  | [optional] [default to 0] |
| **limit** | **float**|  | [optional] [default to 1000] |

### Return type

[**\OpenAPI\Client\Model\DatevBookingsResponse**](../Model/DatevBookingsResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingDatevBookingGet()`

```php
apiV1InternalAccountingDatevBookingGet($startDate, $endDate, $withEnshrined, $sDTracingId, $offset, $limit): \OpenAPI\Client\Model\DatevBookingsResponse
```

Returns all payment bookings and accounting records with the relevant information which is used for the export to DATEV for a client in the given time range. Revenue bookings are loaded using document date, payment bookings are loaded using the pay date.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$startDate = 2022-03-01; // string
$endDate = 2022-04-01; // string
$withEnshrined = true; // bool
$sDTracingId = 'sDTracingId_example'; // string
$offset = 1000; // float
$limit = 10; // float

try {
    $result = $apiInstance->apiV1InternalAccountingDatevBookingGet($startDate, $endDate, $withEnshrined, $sDTracingId, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DatevApi->apiV1InternalAccountingDatevBookingGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **startDate** | **string**|  | |
| **endDate** | **string**|  | |
| **withEnshrined** | **bool**|  | [default to false] |
| **sDTracingId** | **string**|  | [optional] |
| **offset** | **float**|  | [optional] [default to 0] |
| **limit** | **float**|  | [optional] [default to 1000] |

### Return type

[**\OpenAPI\Client\Model\DatevBookingsResponse**](../Model/DatevBookingsResponse.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
