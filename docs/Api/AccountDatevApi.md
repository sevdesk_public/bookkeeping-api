# OpenAPI\Client\AccountDatevApi

All URIs are relative to http://localhost:8080, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**apiV1InternalAccountingAccountsGet()**](AccountDatevApi.md#apiV1InternalAccountingAccountsGet) | **GET** /api/v1/_internal/accounting/accounts | Use this endpoint for retrieving all valid accounts for specified tax-rules, document-types, chart of accounts and small-business-owners. |
| [**apiV1InternalAccountingAccountsPaymentGet()**](AccountDatevApi.md#apiV1InternalAccountingAccountsPaymentGet) | **GET** /api/v1/_internal/accounting/accounts/payment | Use this endpoint for retrieving all supported payment accounts for a specified chart of accounts. The result is sorted by category (Bankkonto, Postbank, Verrechnungskonto) and then by account number ascending. |
| [**apiV1InternalAccountingAccountsSkrmappingGet()**](AccountDatevApi.md#apiV1InternalAccountingAccountsSkrmappingGet) | **GET** /api/v1/_internal/accounting/accounts/skrmapping | Use this endpoint for map a given account number of a source chartOfAccounts to a target chartOfAccounts (skr03 -&gt; skr04 or skr04 to skr03). |
| [**apiV1InternalAccountingAccountsTaxrulesGet()**](AccountDatevApi.md#apiV1InternalAccountingAccountsTaxrulesGet) | **GET** /api/v1/_internal/accounting/accounts/taxrules | Use this endpoint for retrieving all valid taxRules for a specified account number, depending on whether  small-business-owner rules or regular tax-rules have to be returned. |
| [**apiV1InternalAccountingChartOfAccountsAccountsDepreciationaccountAssetAccountNumberGet()**](AccountDatevApi.md#apiV1InternalAccountingChartOfAccountsAccountsDepreciationaccountAssetAccountNumberGet) | **GET** /api/v1/_internal/accounting/{chartOfAccounts}/accounts/depreciationaccount/{assetAccountNumber} | Use this endpoint to retrieve the depreciation account for a specified asset account and chart of accounts. |
| [**apiV1InternalAccountingChartOfAccountsAccountsGuidanceGet()**](AccountDatevApi.md#apiV1InternalAccountingChartOfAccountsAccountsGuidanceGet) | **GET** /api/v1/_internal/accounting/{chartOfAccounts}/accounts/guidance | This endpoint can be used to obtain guidance on creating bookable documents that are compliant to our booking system. A guide describes the compatibility of an account and document types in combination with tax rules and tax rates. |


## `apiV1InternalAccountingAccountsGet()`

```php
apiV1InternalAccountingAccountsGet($sDTracingId, $taxRules, $documentType, $chartOfAccounts, $smallBusinessOwner): \OpenAPI\Client\Model\AccountDatevExternalInner[]
```

Use this endpoint for retrieving all valid accounts for specified tax-rules, document-types, chart of accounts and small-business-owners.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountDatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$taxRules = array(new \OpenAPI\Client\Model\\OpenAPI\Client\Model\AccountDatevExternalTaxRules()); // \OpenAPI\Client\Model\AccountDatevExternalTaxRules[] | The tax-rules for which usable accounts are to be returned.
$documentType = array(new \OpenAPI\Client\Model\\OpenAPI\Client\Model\DocumentType()); // \OpenAPI\Client\Model\DocumentType[] | The document-types for which usable accounts are to be returned.
$chartOfAccounts = DATEV_SKR_04; // string | The chart-of-accounts for which usable accounts are to be returned.
$smallBusinessOwner = True; // bool | Describes if accounts usable for small business owners (small settlement) are to be returned.

try {
    $result = $apiInstance->apiV1InternalAccountingAccountsGet($sDTracingId, $taxRules, $documentType, $chartOfAccounts, $smallBusinessOwner);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountDatevApi->apiV1InternalAccountingAccountsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sDTracingId** | **string**|  | [optional] |
| **taxRules** | [**\OpenAPI\Client\Model\AccountDatevExternalTaxRules[]**](../Model/\OpenAPI\Client\Model\AccountDatevExternalTaxRules.md)| The tax-rules for which usable accounts are to be returned. | [optional] |
| **documentType** | [**\OpenAPI\Client\Model\DocumentType[]**](../Model/\OpenAPI\Client\Model\DocumentType.md)| The document-types for which usable accounts are to be returned. | [optional] |
| **chartOfAccounts** | **string**| The chart-of-accounts for which usable accounts are to be returned. | [optional] |
| **smallBusinessOwner** | **bool**| Describes if accounts usable for small business owners (small settlement) are to be returned. | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountDatevExternalInner[]**](../Model/AccountDatevExternalInner.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingAccountsPaymentGet()`

```php
apiV1InternalAccountingAccountsPaymentGet($chartOfAccounts, $sDTracingId): \OpenAPI\Client\Model\PaymentAccountsInner[]
```

Use this endpoint for retrieving all supported payment accounts for a specified chart of accounts. The result is sorted by category (Bankkonto, Postbank, Verrechnungskonto) and then by account number ascending.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountDatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$chartOfAccounts = DATEV_SKR_04; // string
$sDTracingId = 'sDTracingId_example'; // string

try {
    $result = $apiInstance->apiV1InternalAccountingAccountsPaymentGet($chartOfAccounts, $sDTracingId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountDatevApi->apiV1InternalAccountingAccountsPaymentGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **chartOfAccounts** | **string**|  | |
| **sDTracingId** | **string**|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\PaymentAccountsInner[]**](../Model/PaymentAccountsInner.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingAccountsSkrmappingGet()`

```php
apiV1InternalAccountingAccountsSkrmappingGet($accountNumber, $sourceChartOfAccounts, $targetChartOfAccounts, $sDTracingId): \OpenAPI\Client\Model\SkrMappingResult
```

Use this endpoint for map a given account number of a source chartOfAccounts to a target chartOfAccounts (skr03 -> skr04 or skr04 to skr03).

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountDatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accountNumber = 1800; // int
$sourceChartOfAccounts = DATEV_SKR_04; // string
$targetChartOfAccounts = DATEV_SKR_03; // string
$sDTracingId = 'sDTracingId_example'; // string

try {
    $result = $apiInstance->apiV1InternalAccountingAccountsSkrmappingGet($accountNumber, $sourceChartOfAccounts, $targetChartOfAccounts, $sDTracingId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountDatevApi->apiV1InternalAccountingAccountsSkrmappingGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **accountNumber** | **int**|  | |
| **sourceChartOfAccounts** | **string**|  | |
| **targetChartOfAccounts** | **string**|  | |
| **sDTracingId** | **string**|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\SkrMappingResult**](../Model/SkrMappingResult.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingAccountsTaxrulesGet()`

```php
apiV1InternalAccountingAccountsTaxrulesGet($accountNumber, $sDTracingId, $smallBusinessOwner): \OpenAPI\Client\Model\AccountValidTaxRuleInformation
```

Use this endpoint for retrieving all valid taxRules for a specified account number, depending on whether  small-business-owner rules or regular tax-rules have to be returned.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountDatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accountNumber = 4000; // int | The account-number for which tax-rules have to be retrieved.
$sDTracingId = 'sDTracingId_example'; // string
$smallBusinessOwner = True; // bool | Describes if small-business-owner tax-rules or regular tax-rules are returned.  If left empty, regular tax-rules are returned.

try {
    $result = $apiInstance->apiV1InternalAccountingAccountsTaxrulesGet($accountNumber, $sDTracingId, $smallBusinessOwner);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountDatevApi->apiV1InternalAccountingAccountsTaxrulesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **accountNumber** | **int**| The account-number for which tax-rules have to be retrieved. | |
| **sDTracingId** | **string**|  | [optional] |
| **smallBusinessOwner** | **bool**| Describes if small-business-owner tax-rules or regular tax-rules are returned.  If left empty, regular tax-rules are returned. | [optional] |

### Return type

[**\OpenAPI\Client\Model\AccountValidTaxRuleInformation**](../Model/AccountValidTaxRuleInformation.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingChartOfAccountsAccountsDepreciationaccountAssetAccountNumberGet()`

```php
apiV1InternalAccountingChartOfAccountsAccountsDepreciationaccountAssetAccountNumberGet($chartOfAccounts, $assetAccountNumber, $sDTracingId): int
```

Use this endpoint to retrieve the depreciation account for a specified asset account and chart of accounts.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountDatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$chartOfAccounts = skr04; // string
$assetAccountNumber = 670; // float
$sDTracingId = 'sDTracingId_example'; // string

try {
    $result = $apiInstance->apiV1InternalAccountingChartOfAccountsAccountsDepreciationaccountAssetAccountNumberGet($chartOfAccounts, $assetAccountNumber, $sDTracingId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountDatevApi->apiV1InternalAccountingChartOfAccountsAccountsDepreciationaccountAssetAccountNumberGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **chartOfAccounts** | **string**|  | |
| **assetAccountNumber** | **float**|  | |
| **sDTracingId** | **string**|  | [optional] |

### Return type

**int**

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `apiV1InternalAccountingChartOfAccountsAccountsGuidanceGet()`

```php
apiV1InternalAccountingChartOfAccountsAccountsGuidanceGet($chartOfAccounts, $taxationScope, $sDTracingId): \OpenAPI\Client\Model\Guidance
```

This endpoint can be used to obtain guidance on creating bookable documents that are compliant to our booking system. A guide describes the compatibility of an account and document types in combination with tax rules and tax rates.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountDatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$chartOfAccounts = 'chartOfAccounts_example'; // string | Defines the chart-of-accounts the account guidance should be described for.
$taxationScope = new \OpenAPI\Client\Model\TaxationScope(); // TaxationScope | Defines the taxation scope of the account guidance should be described for.
$sDTracingId = 'sDTracingId_example'; // string

try {
    $result = $apiInstance->apiV1InternalAccountingChartOfAccountsAccountsGuidanceGet($chartOfAccounts, $taxationScope, $sDTracingId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountDatevApi->apiV1InternalAccountingChartOfAccountsAccountsGuidanceGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **chartOfAccounts** | **string**| Defines the chart-of-accounts the account guidance should be described for. | |
| **taxationScope** | [**TaxationScope**](../Model/.md)| Defines the taxation scope of the account guidance should be described for. | |
| **sDTracingId** | **string**|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\Guidance**](../Model/Guidance.md)

### Authorization

[jwtAuthentication](../../README.md#jwtAuthentication)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
