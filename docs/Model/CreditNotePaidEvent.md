# # CreditNotePaidEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountingContact** | [**\OpenAPI\Client\Model\Creditor**](Creditor.md) |  |
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  |
**creditNote** | [**\OpenAPI\Client\Model\CreditNote**](CreditNote.md) |  |
**transaction** | [**\OpenAPI\Client\Model\Transaction**](Transaction.md) |  |
**paymentDifference** | [**\OpenAPI\Client\Model\PaymentDifference**](PaymentDifference.md) |  |
**recipientCountry** | [**\OpenAPI\Client\Model\CountryCodeEnum**](CountryCodeEnum.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
