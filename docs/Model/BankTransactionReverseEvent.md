# # BankTransactionReverseEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | **float** | Reference of sev-client. This numerical value is usually the v4 entity id. |
**transaction** | **float** | Reference of bank transaction. This numerical value is usually the v4 entity id. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
