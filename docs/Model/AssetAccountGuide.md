# # AssetAccountGuide

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountGuideType** | **string** |  | [optional]
**accountId** | **int** |  |
**accountNumber** | **int** |  |
**accountName** | **string** |  |
**accountDescription** | **string** |  | [optional]
**chartOfAccounts** | [**\OpenAPI\Client\Model\ChartOfAccounts**](ChartOfAccounts.md) |  |
**accountCategory** | **int** |  |
**accountType** | [**\OpenAPI\Client\Model\AccountType**](AccountType.md) |  |
**defaultVisibility** | **string** |  |
**allowedDocumentTypes** | **string[]** |  |
**allowedTaxRules** | [**\OpenAPI\Client\Model\TaxRule[]**](TaxRule.md) |  |
**allowedPaymentDifferenceReasons** | [**\OpenAPI\Client\Model\PaymentDifferenceReason[]**](PaymentDifferenceReason.md) |  |
**assetAccountBoundaries** | [**\OpenAPI\Client\Model\AssetAccountBoundaries[]**](AssetAccountBoundaries.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
