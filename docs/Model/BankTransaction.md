# # BankTransaction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference** | **float** | Reference of bank transaction. This numerical value is usually the v4 entity id. |
**amount** | **float** | Transaction amount in euro cent. |
**date** | **\DateTime** | Transaction date in ISO-8601. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
