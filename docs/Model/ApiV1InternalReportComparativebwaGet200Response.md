# # ApiV1InternalReportComparativebwaGet200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional]
**period** | **string** |  | [optional]
**comparativePeriod** | **string** |  | [optional]
**rows** | [**\OpenAPI\Client\Model\ComparativeBwaRow[]**](ComparativeBwaRow.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
