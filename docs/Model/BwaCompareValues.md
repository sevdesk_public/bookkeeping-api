# # BwaCompareValues

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstColumn** | **string** |  | [optional]
**secondColumn** | **string** |  | [optional]
**thirdColumn** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
