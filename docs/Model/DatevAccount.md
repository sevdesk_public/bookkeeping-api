# # DatevAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**skr04AccountNumber** | **string** |  | [optional]
**skr03AccountNumber** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
