# # CancellationInvoiceSentEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  |
**invoiceId** | **float** | Id der zu stornierenden Rechnung |
**cancellationInvoiceId** | **float** | Id der erstellten Stornorechnung |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
