# # ReceiptRegisteredPositionsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [default to 'RECEIPT_TAX_INVERSE']
**datevAccountNumber** | **string** | Defines which tax account the amount shall be booked to. If no account is set, a predefined booking account will be used instead. |
**taxRate** | **float** | Describes a tax rate in percent. Must be positive. |
**totals** | [**\OpenAPI\Client\Model\Totals**](Totals.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
