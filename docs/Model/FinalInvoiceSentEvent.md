# # FinalInvoiceSentEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**debitor** | [**\OpenAPI\Client\Model\Debitor**](Debitor.md) |  |
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  |
**finalInvoice** | [**\OpenAPI\Client\Model\FinalInvoiceSent**](FinalInvoiceSent.md) |  |
**advanceInvoices** | **float[]** |  |
**partialInvoices** | **float[]** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
