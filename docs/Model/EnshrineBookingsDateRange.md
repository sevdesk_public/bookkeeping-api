# # EnshrineBookingsDateRange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  | [optional]
**startDate** | **float** | Start date of immutability change. | [optional]
**endDate** | **float** | End date of immutability change. | [optional]
**allPayments** | **bool** |  | [optional]
**documentTypes** | **string[]** |  | [optional]
**documentType** | **string** |  | [optional]
**documentId** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
