# # BookingReversedEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  |
**documentId** | **float** | Id des umzukehrenden Dokuments |
**documentType** | **string** | Type des umzukehrenden Dokuments |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
