# # TotalsWithClaim

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**net** | **float** | Net amount (i.e. without Tax). Must be positive. |
**gross** | **float** | Gross amount (i.e. including Tax). Must be positive. |
**tax** | **float** | Tax amount. Has to be positive for positions, can be negative for invoices. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
