# # CompanySettingsEventResponseValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**taxationType** | **string** |  | [optional]
**profitAssessmentType** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
