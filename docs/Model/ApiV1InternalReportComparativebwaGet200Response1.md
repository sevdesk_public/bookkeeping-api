# # ApiV1InternalReportComparativebwaGet200Response1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional]
**period** | **string** |  | [optional]
**rows** | [**\OpenAPI\Client\Model\BwaRow[]**](BwaRow.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
