# # VatRate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**taxRate** | **float** | The VAT rate percentage (e.g., 19.0 for 19%). | [optional]
**vatType** | **string** | The type of VAT rate (e.g., standard, reduced, etc.). | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
