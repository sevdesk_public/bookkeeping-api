# # PaymentDetachedEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  |
**documentId** | **float** | Id of the edited document |
**documentType** | **string** | Type of the edited document |
**transactionId** | **float** | Id of the transaction |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
