# # TwoRowsWithOneTaxableBaseAndTwoTaxFields

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**taxableBaseFieldNumber** | **string** |  |
**firstTaxFieldNumber** | **string** |  |
**secondTaxFieldNumber** | **string** |  |
**description** | **string** |  |
**kzFieldGroupType** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
