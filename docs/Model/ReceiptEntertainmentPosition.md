# # ReceiptEntertainmentPosition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [default to 'RECEIPT']
**taxRate** | **float** | Describes a tax rate in percent. Must be positive. |
**totals** | [**\OpenAPI\Client\Model\Totals**](Totals.md) |  |
**datevAccountNumber** | **string** | Specifies the individual account to which the amount should be recorded. For entertainment receipts only 6640 and 6644 are allowed. | [optional]
**occasionType** | **string** |  |
**isInverse** | **bool** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
