# # DatevBookingsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **float** |  | [optional]
**limit** | **float** |  | [optional]
**total** | **float** |  | [optional]
**entries** | [**\OpenAPI\Client\Model\DatevBooking[]**](DatevBooking.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
