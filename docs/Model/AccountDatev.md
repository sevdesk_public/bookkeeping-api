# # AccountDatev

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**skr04AccountNumber** | **string** |  | [optional]
**skr04AccountName** | **string** |  | [optional]
**skr03AccountNumber** | **string** |  | [optional]
**skr03AccountName** | **string** |  | [optional]
**isExpenseAccount** | **bool** |  | [optional]
**isRevenueAccount** | **bool** |  | [optional]
**isAssetAccount** | **bool** |  | [optional]
**validFromDate** | **string** |  | [optional]
**validUntilDate** | **string** |  | [optional]
**bookingKey** | [**\OpenAPI\Client\Model\AccountDatevBookingKey**](AccountDatevBookingKey.md) |  | [optional]
**balanceSide** | **string** |  | [optional]
**isAutomaticAccount** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
