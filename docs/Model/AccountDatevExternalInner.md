# # AccountDatevExternalInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountDatevId** | **int** |  | [optional]
**accountDatevNumber** | **int** |  | [optional]
**accountDatevName** | **string** |  | [optional]
**accountDatevDescription** | **string** |  | [optional]
**accountDatevTaxRates** | **float[]** |  | [optional]
**accountDatevCategory** | **int** |  | [optional]
**accountDatevType** | **string[]** |  | [optional]
**isAssetAccount** | **bool** |  | [optional]
**isEntertainmentReceipt** | **bool** |  | [optional]
**accountDatevUsageLimitations** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
