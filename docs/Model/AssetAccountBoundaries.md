# # AssetAccountBoundaries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetType** | **string** |  |
**amountLimits** | [**\OpenAPI\Client\Model\AssetAccountBoundariesAmountLimits**](AssetAccountBoundariesAmountLimits.md) |  |
**allowedUsefulLives** | **float[]** |  |
**validFrom** | **string** | Date in yyyy-mm-dd format |
**validUntil** | **string** | Date in yyyy-mm-dd format |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
