# # InvoiceSentEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**debitor** | [**\OpenAPI\Client\Model\Debitor**](Debitor.md) |  |
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  |
**invoice** | [**\OpenAPI\Client\Model\InvoiceSent**](InvoiceSent.md) |  |
**recipientCountry** | [**\OpenAPI\Client\Model\CountryCodeEnum**](CountryCodeEnum.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
