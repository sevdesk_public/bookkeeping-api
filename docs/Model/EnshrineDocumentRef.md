# # EnshrineDocumentRef

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documentId** | **float** |  | [optional]
**documentType** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
