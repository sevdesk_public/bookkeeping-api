# # OriginDocument

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documentRef** | [**\OpenAPI\Client\Model\DocumentRef**](DocumentRef.md) |  | [optional]
**subtype** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
