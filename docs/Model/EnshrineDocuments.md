# # EnshrineDocuments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  | [optional]
**documentRefs** | [**\OpenAPI\Client\Model\EnshrineDocumentRef[]**](EnshrineDocumentRef.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
