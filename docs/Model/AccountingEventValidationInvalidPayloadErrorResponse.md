# # AccountingEventValidationInvalidPayloadErrorResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **string** | the error code specifies the type of exception which occurred. | [optional]
**errorMessage** | **string** | the error message gives additional information about the error case. | [optional]
**domainErrorCode** | [**\OpenAPI\Client\Model\DomainErrorCodes**](DomainErrorCodes.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
