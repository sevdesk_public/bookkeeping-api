# # Transaction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checkAccountTransactionLogId** | **float** | ID of the CheckAccountTransactionLog. Required for FE and accounting reports. |
**date** | **float** | payment date as Unix Timestamp |
**amount** | **float** | payed amount |
**targetAccountNumber** | **float** | Skr04 Datev account (1800-1859) or Skr04 Datev bank account (1600-1629) |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
