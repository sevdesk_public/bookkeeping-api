# # ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | **float** | Reference of sev-client. This numerical value is usually the v4 entity id. |
**bankAccountNumber** | [**\OpenAPI\Client\Model\SkrAccount**](SkrAccount.md) |  |
**transaction** | [**\OpenAPI\Client\Model\BankTransaction**](BankTransaction.md) |  |
**eventType** | **string** | Discriminator type for banking events. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
