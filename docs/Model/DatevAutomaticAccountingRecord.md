# # DatevAutomaticAccountingRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional]
**fromAccount** | [**\OpenAPI\Client\Model\DatevAccount**](DatevAccount.md) |  | [optional]
**toAccount** | [**\OpenAPI\Client\Model\DatevAccount**](DatevAccount.md) |  | [optional]
**amount** | **float** |  | [optional]
**taxRate** | **float** |  | [optional]
**regularAccountingRecordId** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
