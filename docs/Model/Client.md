# # Client

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | Unique id of the sevDesk user |
**taxationType** | **string** | Taxation type of the company |
**profitAssessmentType** | **string** | Profit assessment type of the company |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
