# # ReceiptEntertainmentPaidEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creditor** | [**\OpenAPI\Client\Model\Creditor**](Creditor.md) |  |
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  |
**receipt** | [**\OpenAPI\Client\Model\ReceiptEntertainment**](ReceiptEntertainment.md) |  |
**transaction** | [**\OpenAPI\Client\Model\Transaction**](Transaction.md) |  |
**paymentDifference** | [**\OpenAPI\Client\Model\PaymentDifference**](PaymentDifference.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
