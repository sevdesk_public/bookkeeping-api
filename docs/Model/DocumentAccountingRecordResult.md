# # DocumentAccountingRecordResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **string** |  | [optional]
**deliveryDate** | **string** |  | [optional]
**documentDate** | **string** |  | [optional]
**payDate** | **string** |  | [optional]
**enshrineDate** | **string** |  | [optional]
**isRevenue** | **bool** |  | [optional]
**checkAccountTransactionId** | **int** |  | [optional]
**generalReversalOriginBooking** | **int** |  | [optional]
**regularAccountingRecords** | [**\OpenAPI\Client\Model\DocumentAccountingRecordPosition[]**](DocumentAccountingRecordPosition.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
