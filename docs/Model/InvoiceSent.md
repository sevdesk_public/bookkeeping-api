# # InvoiceSent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | ID of the document in seVDesk. |
**taxRuleCode** | **string** | Allowed TaxRule values (selected by creator of the document): - USTPFL_UMS_EINN: Pflichtige Umsätze bspw. mit 0%, 7% oder 19% MwSt. - INNERGEM_LIEF: Innergemeinschaftliche Lieferungen - AUSFUHREN: Steuerfreie Ausfuhrlieferung - REV_CHARGE_13B_1: Reverse Charge nach §13b Abs. 1 UStG - REV_CHARGE_13B_2_1_BTOB: Reverse Charge nach §13b Abs. 2 UStG (Business to Business) - REV_CHARGE_13B_2_1_BTOC: Reverse Charge nach §13b Abs. 2 UStG (Business to Customer) - STFREIE_UMS_P4: Steuerfreie Umsätze nach § 4 UStG - KLEINUNTERNEHMER_P19: Umsätze als Kleinunternehmer - NICHT_VORST_ABZUGSF_AUFW: Nicht vorsteuerabzugsfähige Aufwendungen - VORST_ABZUGSF_AUFW: Vorsteuerabzugsfähige Aufwendungen - NICHT_IM_INLAND_STEUERBAR: Nicht im Inland steuerbare Leistung - OSS_GOODS: One-Stop-Shop (OSS) - Goods - OSS_SERVICES: One-Stop-Shop (OSS) - Electronic services - OSS_OTHER: One-Stop-Shop (OSS) - Other services |
**deliveryDate** | **float** | delivery date as Unix Timestamp |
**documentDate** | **float** | invoice date as Unix Timestamp |
**totals** | [**\OpenAPI\Client\Model\Totals**](Totals.md) |  |
**discountGross** | **float** | Total discount in Euro | [optional]
**extraChargeGross** | **float** | Total extra charge in Euro | [optional]
**positions** | [**\OpenAPI\Client\Model\InvoicePaidPositionsInner[]**](InvoicePaidPositionsInner.md) |  |
**isNet** | **bool** | Describes whether the document is net or gross. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
