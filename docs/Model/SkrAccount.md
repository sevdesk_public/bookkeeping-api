# # SkrAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chartOfAccounts** | **string** |  |
**accountNumber** | **int** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
