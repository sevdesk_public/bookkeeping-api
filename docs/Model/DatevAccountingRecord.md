# # DatevAccountingRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromAccount** | [**\OpenAPI\Client\Model\DatevAccount**](DatevAccount.md) |  | [optional]
**toAccount** | [**\OpenAPI\Client\Model\DatevAccount**](DatevAccount.md) |  | [optional]
**amount** | **float** |  | [optional]
**taxRate** | **float** |  | [optional]
**fromAccountingContactNumber** | **float** |  | [optional]
**toAccountingContactNumber** | **float** |  | [optional]
**fromBookingKeyNumber** | **float** |  | [optional]
**toBookingKeyNumber** | **float** |  | [optional]
**ruleName** | **string** |  | [optional]
**automaticAccountingRecord** | [**\OpenAPI\Client\Model\DatevAutomaticAccountingRecord**](DatevAutomaticAccountingRecord.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
