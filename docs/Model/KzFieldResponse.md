# # KzFieldResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kzFieldGroups** | [**\OpenAPI\Client\Model\KzFieldResponseKzFieldGroupsInner[]**](KzFieldResponseKzFieldGroupsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
