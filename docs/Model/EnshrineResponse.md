# # EnshrineResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookingsEnshrined** | **array<string,float>** | key is a string and can be one of the following values (\&quot;Invoice\&quot;, \&quot;Voucher\&quot;, \&quot;CreditNote\&quot;, \&quot;Asset\&quot;) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
