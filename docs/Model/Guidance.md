# # Guidance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountGuides** | [**\OpenAPI\Client\Model\GuidanceAccountGuidesInner[]**](GuidanceAccountGuidesInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
