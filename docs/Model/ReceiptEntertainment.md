# # ReceiptEntertainment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | ID of the document in seVDesk. |
**taxRuleCode** | **string** | Allowed TaxRule values (selected by creator of the document): - VORST_ABZUGSF_AUFW: Vorsteuerabzugsfähige Aufwendungen - REV_CHARGE_13B_MIT_VORST_ABZUG_0: Reverse Charge gem. §13b Abs. 2 UStG mit Vorsteuerabzug 0% (19%) - REV_CHARGE_13B_OHNE_VORST_ABZUG_0: Reverse Charge gem. §13b UStG ohne Vorsteuerabzug 0% (19%) - REV_CHARGE_13B_EU_0: Reverse Charge gem. §13b Abs. 1 EU Umsätze 0% (19%) - NICHT_VORST_ABZUGSF_AUFW: Nicht vorsteuerabziehbare Aufwendungen - REV_CHARGE_13B_1: Reverse Charge gem. §13b (1) UStG |
**deliveryDate** | **float** | delivery date as Unix Timestamp |
**documentDate** | **float** | document date as Unix Timestamp |
**totals** | [**\OpenAPI\Client\Model\Totals**](Totals.md) |  |
**positions** | [**\OpenAPI\Client\Model\ReceiptEntertainmentPosition[]**](ReceiptEntertainmentPosition.md) |  |
**isNet** | **bool** | Describes whether the document is net or gross. | [default to false]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
