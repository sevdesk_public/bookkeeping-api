# # ReceiptPaid

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | ID of the document in seVDesk. |
**taxRuleCode** | **string** | Allowed TaxRule values (selected by creator of the document): - VORST_ABZUGSF_AUFW: Vorsteuerabzugsfähige Aufwendungen - REV_CHARGE_13B_MIT_VORST_ABZUG_0: Reverse Charge gem. §13b Abs. 2 UStG mit Vorsteuerabzug 0% (19%) - REV_CHARGE_13B_OHNE_VORST_ABZUG_0: Reverse Charge gem. §13b UStG ohne Vorsteuerabzug 0% (19%) - REV_CHARGE_13B_EU_0: Reverse Charge gem. §13b Abs. 1 EU Umsätze 0% (19%) - NICHT_VORST_ABZUGSF_AUFW: Nicht vorsteuerabziehbare Aufwendungen - USTPFL_UMS_EINN: Umsatzsteuerpflichtige Umsätze Einnahmen - INNERGEM_LIEF: Innergemeinschaftliche Lieferungen - INNERGEM_ERWERB: Innergemeinschaftliche Erwerbe - AUSFUHREN: Ausfuhren - STFREIE_UMS_P4: Steuerfreie Umsätze §4 UStG - REV_CHARGE_13B_1: Reverse Charge gem. §13b (1) UStG - KLEINUNTERNEHMER_P19: Umsätze als Kleinunternehmer - NICHT_STEUERBAR_TAX: Umsatzsteuerrechtlich nicht relevant - NICHT_STEUERBAR_EXPENSE: Umsatzsteuerrechtlich nicht relevant (Für Ausgabebelege) - NICHT_IM_INLAND_STEUERBAR: Nicht im Inland steuerbare Leistung |
**deliveryDate** | **float** | delivery date as Unix Timestamp |
**documentDate** | **float** | document date as Unix Timestamp |
**totals** | [**\OpenAPI\Client\Model\Totals**](Totals.md) |  |
**positions** | [**\OpenAPI\Client\Model\ReceiptPaidPositionsInner[]**](ReceiptPaidPositionsInner.md) |  |
**isNet** | **bool** | Describes whether the document is net or gross. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
