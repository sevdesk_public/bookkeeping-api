# # CompanySettingRebookingParameters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**\OpenAPI\Client\Model\Client**](Client.md) |  | [optional]
**startDate** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
