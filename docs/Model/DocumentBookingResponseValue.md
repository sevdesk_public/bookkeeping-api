# # DocumentBookingResponseValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hasBookings** | **bool** |  | [optional]
**latestEnshrineDate** | **string** |  | [optional]
**allowTaxationTypeChangeFromKU** | **bool** | If true, taxationType change is allowed although actual validation prohibits it. | [optional]
**documents** | [**\OpenAPI\Client\Model\NotEnshrinedDocuments[]**](NotEnshrinedDocuments.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
