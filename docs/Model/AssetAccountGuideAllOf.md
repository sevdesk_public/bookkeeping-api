# # AssetAccountGuideAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetAccountBoundaries** | [**\OpenAPI\Client\Model\AssetAccountBoundaries[]**](AssetAccountBoundaries.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
