# # BwaRow

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rowNumber** | **int** |  | [optional]
**name** | **string** |  | [optional]
**total** | **string** |  | [optional]
**children** | [**\OpenAPI\Client\Model\BwaRow[]**](BwaRow.md) | Nested list of BwaRow, at most 3 levels deep | [optional]
**hasBookedAccounts** | **bool** |  | [optional]
**clarificationTranslationKey** | **string** |  | [optional]
**percentages** | [**\OpenAPI\Client\Model\BwaCompareValues**](BwaCompareValues.md) |  | [optional]
**rowStyling** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
