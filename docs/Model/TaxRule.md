# # TaxRule

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  |
**code** | **string** |  |
**taxRates** | [**\OpenAPI\Client\Model\TaxRateEnum[]**](TaxRateEnum.md) |  |
**taxationScope** | [**\OpenAPI\Client\Model\TaxationScope**](TaxationScope.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
