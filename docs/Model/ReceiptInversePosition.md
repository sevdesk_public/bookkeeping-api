# # ReceiptInversePosition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [default to 'RECEIPT_INVERSE']
**datevAccountNumber** | **string** | Specifies the individual revenue or income account to which the amount should be recorded. If no account is specified, the amount will be recorded in a default account. |
**taxRate** | **float** | Describes a tax rate in percent. Must be positive. |
**totals** | [**\OpenAPI\Client\Model\Totals**](Totals.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
