# # SkrMappingResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chartOfAccounts** | **string** |  | [optional]
**accountNumber** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
