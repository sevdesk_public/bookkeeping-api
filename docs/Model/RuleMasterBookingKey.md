# # RuleMasterBookingKey

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The ID of the Booking Key | [optional]
**name** | **string** | name of key | [optional]
**taxRate** | **float** |  | [optional]
**number** | **int** | Unique number for the Booking Key | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
