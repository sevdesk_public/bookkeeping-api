# # DatevBooking

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**\OpenAPI\Client\Model\DatevBookingClient**](DatevBookingClient.md) |  | [optional]
**documentDate** | **float** |  | [optional]
**payDate** | **float** |  | [optional]
**documentRef** | [**\OpenAPI\Client\Model\DatevBookingDocumentRef**](DatevBookingDocumentRef.md) |  | [optional]
**checkAccountTransactionId** | **float** |  | [optional]
**isRevenue** | **bool** |  | [optional]
**regularAccountingRecords** | [**\OpenAPI\Client\Model\DatevAccountingRecord[]**](DatevAccountingRecord.md) |  | [optional]
**enshrineDate** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
