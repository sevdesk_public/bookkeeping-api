# # DocumentAccountingRecordPosition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromAccount** | [**\OpenAPI\Client\Model\AccountDatev**](AccountDatev.md) |  | [optional]
**toAccount** | [**\OpenAPI\Client\Model\AccountDatev**](AccountDatev.md) |  | [optional]
**amount** | **float** |  | [optional]
**taxRate** | **float** |  | [optional]
**fromAccountingContact** | [**\OpenAPI\Client\Model\AccountingContact**](AccountingContact.md) |  | [optional]
**toAccountingContact** | [**\OpenAPI\Client\Model\AccountingContact**](AccountingContact.md) |  | [optional]
**fromBookingKey** | [**\OpenAPI\Client\Model\BookingKey**](BookingKey.md) |  | [optional]
**toBookingKey** | [**\OpenAPI\Client\Model\BookingKey**](BookingKey.md) |  | [optional]
**ruleName** | **string** |  | [optional]
**automaticAccountingRecord** | [**\OpenAPI\Client\Model\DocumentAccountingRecordPositionAutomaticAccountingRecord**](DocumentAccountingRecordPositionAutomaticAccountingRecord.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
