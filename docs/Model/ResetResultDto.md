# # ResetResultDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deletedBookingJobs** | **float** | Amount of deleted booking jobs. |
**deletedBookings** | **float** | Amount of deleted bookings. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
