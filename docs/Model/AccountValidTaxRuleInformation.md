# # AccountValidTaxRuleInformation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allowedTaxRules** | [**\OpenAPI\Client\Model\AccountDatevExternalTaxRules[]**](AccountDatevExternalTaxRules.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
