# # CreditNote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | ID of the document in seVDesk. |
**taxRuleCode** | **string** | Allowed TaxRule values (selected by creator of the document): - REV_CHARGE_13B_MIT_VORST_ABZUG_0: Reverse Charge gem. §13b Abs. 2 UStG mit Vorsteuerabzug 0% (19%) - REV_CHARGE_13B_OHNE_VORST_ABZUG_0: Reverse Charge gem. §13b UStG ohne Vorsteuerabzug 0% (19%) - REV_CHARGE_13B_EU_0: Reverse Charge gem. §13b Abs. 1 EU Umsätze 0% (19%) - NICHT_VORST_ABZUGSF_AUFW: Nicht vorsteuerabziehbare Aufwendungen |
**deliveryDate** | **float** | delivery date as Unix Timestamp |
**documentDate** | **float** | document date as Unix Timestamp |
**totals** | [**\OpenAPI\Client\Model\Totals**](Totals.md) |  |
**discountGross** | **float** | Total discount in Euro | [optional]
**extraChargeGross** | **float** | Total extra charge in Euro | [optional]
**positions** | [**\OpenAPI\Client\Model\CreditNotePositionsInner[]**](CreditNotePositionsInner.md) |  |
**isNet** | **bool** | Describes whether the document is net or gross. |
**categoryType** | **string** | Booking category of the credit note |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
