# # DocumentAccountingRecordPositionAutomaticAccountingRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fromAccount** | [**\OpenAPI\Client\Model\AccountDatev**](AccountDatev.md) |  | [optional]
**toAccount** | [**\OpenAPI\Client\Model\AccountDatev**](AccountDatev.md) |  | [optional]
**amount** | **float** |  | [optional]
**taxRate** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
