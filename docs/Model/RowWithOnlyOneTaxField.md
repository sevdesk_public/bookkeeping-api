# # RowWithOnlyOneTaxField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**taxFieldNumber** | **string** |  |
**description** | **string** |  |
**kzFieldGroupType** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
