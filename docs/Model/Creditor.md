# # Creditor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | ID of the AccountingContact in sevDesk. Required for FE, accounting reports and changes to creditor number. |
**accountNumber** | **float** | Creditor account number |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
