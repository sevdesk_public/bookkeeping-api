# # PaymentDifference

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Represents the type of the payment difference:  Allowed values: - PARTIAL_PAYMENT: nicht buchungsrelevant (Unterzahlung) - CASH_DISCOUNT: wirkt sich auf Positionen aus (Unterzahlung) - OTHER: wirkt sich auf Positionen aus (Unterzahlung) - REMINDER: wird extra verbucht (Überzahlung) - COSTS_OF_PAYMENT_SERVICE_PROVIDER: wird extra verbucht (Unterzahlung) - NONE: es gibt keine Zahlungsdifferenz (amount muss 0 sein) - COLLECTIVE: nicht buchungsrelevant (Überzahlung) |
**amount** | **float** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
