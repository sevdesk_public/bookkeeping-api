# # Depreciation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | ID of the depreciation in sevdesk. Is required for later processing (e.g. display in the FE, report calculation) |
**depreciationDate** | **float** | Depreciation date as Unix Timestamp |
**amount** | **float** | Depreciation amount. Has to be positive. |
**assetAccountNumber** | **string** |  |
**assetReceiptRef** | [**\OpenAPI\Client\Model\DocumentRef**](DocumentRef.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
