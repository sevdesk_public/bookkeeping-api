# # ComparativeBwaRow

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rowNumber** | **int** |  | [optional]
**name** | **string** |  | [optional]
**total** | **string** |  | [optional]
**comparativeTotal** | **string** |  | [optional]
**children** | [**\OpenAPI\Client\Model\ComparativeBwaRow[]**](ComparativeBwaRow.md) | Nested list of ComparativeBwaRow, at most 3 levels deep | [optional]
**hasBookedAccounts** | **bool** |  | [optional]
**clarificationTranslationKey** | **string** |  | [optional]
**percentages** | [**\OpenAPI\Client\Model\BwaCompareValues**](BwaCompareValues.md) |  | [optional]
**comparativePercentages** | [**\OpenAPI\Client\Model\BwaCompareValues**](BwaCompareValues.md) |  | [optional]
**rowStyling** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
