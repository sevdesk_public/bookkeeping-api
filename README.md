# OpenAPIClient-php

Welcome to the Bookkeeping API of Team Plumbus


## Installation & Usage

### Requirements

PHP 7.4 and later.
Should also work with PHP 8.0.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure Bearer (JWT) authorization: jwtAuthentication
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountDatevApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sDTracingId = 'sDTracingId_example'; // string
$taxRules = array(new \OpenAPI\Client\Model\\OpenAPI\Client\Model\AccountDatevExternalTaxRules()); // \OpenAPI\Client\Model\AccountDatevExternalTaxRules[] | The tax-rules for which usable accounts are to be returned.
$documentType = array(new \OpenAPI\Client\Model\\OpenAPI\Client\Model\DocumentType()); // \OpenAPI\Client\Model\DocumentType[] | The document-types for which usable accounts are to be returned.
$chartOfAccounts = DATEV_SKR_04; // string | The chart-of-accounts for which usable accounts are to be returned.
$smallBusinessOwner = True; // bool | Describes if accounts usable for small business owners (small settlement) are to be returned.

try {
    $result = $apiInstance->apiV1InternalAccountingAccountsGet($sDTracingId, $taxRules, $documentType, $chartOfAccounts, $smallBusinessOwner);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountDatevApi->apiV1InternalAccountingAccountsGet: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *http://localhost:8080*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountDatevApi* | [**apiV1InternalAccountingAccountsGet**](docs/Api/AccountDatevApi.md#apiv1internalaccountingaccountsget) | **GET** /api/v1/_internal/accounting/accounts | Use this endpoint for retrieving all valid accounts for specified tax-rules, document-types, chart of accounts and small-business-owners.
*AccountDatevApi* | [**apiV1InternalAccountingAccountsPaymentGet**](docs/Api/AccountDatevApi.md#apiv1internalaccountingaccountspaymentget) | **GET** /api/v1/_internal/accounting/accounts/payment | Use this endpoint for retrieving all supported payment accounts for a specified chart of accounts. The result is sorted by category (Bankkonto, Postbank, Verrechnungskonto) and then by account number ascending.
*AccountDatevApi* | [**apiV1InternalAccountingAccountsSkrmappingGet**](docs/Api/AccountDatevApi.md#apiv1internalaccountingaccountsskrmappingget) | **GET** /api/v1/_internal/accounting/accounts/skrmapping | Use this endpoint for map a given account number of a source chartOfAccounts to a target chartOfAccounts (skr03 -&gt; skr04 or skr04 to skr03).
*AccountDatevApi* | [**apiV1InternalAccountingAccountsTaxrulesGet**](docs/Api/AccountDatevApi.md#apiv1internalaccountingaccountstaxrulesget) | **GET** /api/v1/_internal/accounting/accounts/taxrules | Use this endpoint for retrieving all valid taxRules for a specified account number, depending on whether  small-business-owner rules or regular tax-rules have to be returned.
*AccountDatevApi* | [**apiV1InternalAccountingChartOfAccountsAccountsDepreciationaccountAssetAccountNumberGet**](docs/Api/AccountDatevApi.md#apiv1internalaccountingchartofaccountsaccountsdepreciationaccountassetaccountnumberget) | **GET** /api/v1/_internal/accounting/{chartOfAccounts}/accounts/depreciationaccount/{assetAccountNumber} | Use this endpoint to retrieve the depreciation account for a specified asset account and chart of accounts.
*AccountDatevApi* | [**apiV1InternalAccountingChartOfAccountsAccountsGuidanceGet**](docs/Api/AccountDatevApi.md#apiv1internalaccountingchartofaccountsaccountsguidanceget) | **GET** /api/v1/_internal/accounting/{chartOfAccounts}/accounts/guidance | This endpoint can be used to obtain guidance on creating bookable documents that are compliant to our booking system. A guide describes the compatibility of an account and document types in combination with tax rules and tax rates.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventadvanceinvoicepaidpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/advanceinvoicepaid | Use this endpoint when a advance invoice has been paid and the corresponding bookings are to be generated for it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventAdvanceinvoicepaidValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventadvanceinvoicepaidvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/advanceinvoicepaid/validate | Use this end point to validate an advance invoice before triggering the booking process.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventAdvanceinvoicesentValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventadvanceinvoicesentvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/advanceinvoicesent/validate | Use this endpoint to validate an advance invoice before sending it out.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventBanktransactionBookPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventbanktransactionbookpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/banktransaction/book | Use this endpoint to generate bookings for a bank transaction event.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventBanktransactionReversePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventbanktransactionreversepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/banktransaction/reverse | Use this endpoint to reverse bookings for a bank transaction event.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventBanktransactionValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventbanktransactionvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/banktransaction/validate | Use this endpoint to generate bookings for a bank transaction event.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcancellationinvoicesentpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/cancellationinvoicesent | Use this endpoint when a cancellation invoice has been sent and  the corresponding bookings are to be generated for it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCancellationinvoicesentValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcancellationinvoicesentvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/cancellationinvoicesent/validate | Use this end point when a cancellation invoice has been sent and the corresponding bookings are to be generated.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCreditnotepaidPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcreditnotepaidpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnotepaid | Use this endpoint when a credit note has been paid and the corresponding bookings are to be created.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCreditnotepaidValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcreditnotepaidvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnotepaid/validate | Use this end point to validate a credit note before triggering the booking process.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCreditnotesentPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcreditnotesentpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnotesent | Use this endpoint when a credit note has been sent and the corresponding bookings are to be created.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCreditnotesentValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcreditnotesentvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnotesent/validate | Use this end point to validate a credit note before triggering the booking process.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcreditnoteunderachievementpaidpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnoteunderachievementpaid | Use this endpoint when a credit note underachievement has been paid and the corresponding bookings are to be created.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementpaidValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcreditnoteunderachievementpaidvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnoteunderachievementpaid/validate | Use this end point to validate a credit note underachievement before triggering the booking process.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcreditnoteunderachievementsentpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnoteunderachievementsent | Use this endpoint when a credit note underachievement has been sent and the corresponding bookings are to be created.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventCreditnoteunderachievementsentValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventcreditnoteunderachievementsentvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/creditnoteunderachievementsent/validate | Use this end point to validate a credit note underachievement before triggering the booking process.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventDepreciationPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventdepreciationpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/depreciation | Use this endpoint to create bookings for depreciations.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventDepreciationcorrectionPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventdepreciationcorrectionpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/depreciationcorrection | Use this endpoint to create correction bookings for depreciations.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventfinalinvoicepaidpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/finalinvoicepaid | Use this end point to create the payment bookings for a final invoice.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventFinalinvoicepaidValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventfinalinvoicepaidvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/finalinvoicepaid/validate | Use this end point to validate a final invoice before triggering the booking process.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventFinalinvoicesentPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventfinalinvoicesentpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/finalinvoicesent | Use this end point to create the sent bookings for a final invoice.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventFinalinvoicesentValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventfinalinvoicesentvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/finalinvoicesent/validate | Use this end point to validate a final invoice before sending it out.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventInvoicepaidPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventinvoicepaidpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/invoicepaid | Use this endpoint when an invoice has been paid and the corresponding bookings are to be created.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventInvoicepaidValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventinvoicepaidvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/invoicepaid/validate | Use this end point to validate an invoice before triggering the booking process.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventInvoicesentPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventinvoicesentpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/invoicesent | Use this endpoint when an invoice has been sent and the corresponding bookings are to be created.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventInvoicesentValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventinvoicesentvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/invoicesent/validate | Use this endpoint to validate an invoice before sending it out.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptentertainmentpaidpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptentertainmentpaid | Use this endpoint whenever a receipt with entertainment expenses has been paid  and the corresponding bookings are to be generated for it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptentertainmentpaidValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptentertainmentpaidvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptentertainmentpaid/validate | Use this endpoint when validating an entertainment receipt before paying it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptentertainmentregisteredpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptentertainmentregistered | Use this endpoint whenever a receipt with entertainment expenses has been registered  and the corresponding bookings are to be generated for it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptentertainmentregisteredValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptentertainmentregisteredvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptentertainmentregistered/validate | Use this endpoint when validating an entertainment receipt before registering it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptexpensepaidpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptexpensepaid | Use this endpoint when an expense receipt has been paid  and the corresponding bookings are to be generated for it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptexpensepaidValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptexpensepaidvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptexpensepaid/validate | Use this end point to validate a receipt expense before triggering the booking process.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptexpenseregisteredpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptexpenseregistered | Use this endpoint when an expense receipt has been registered  and the corresponding bookings are to be generated for it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptexpenseregisteredValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptexpenseregisteredvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptexpenseregistered/validate | Use this endpoint when validating an expense receipt before registering it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptrevenuepaidpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptrevenuepaid | Use this endpoint when a receipt revenue has been paid and the corresponding bookings are to be created.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptrevenuepaidValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptrevenuepaidvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptrevenuepaid/validate | Use this endpoint when validating a revenue receipt before paying it.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptrevenueregisteredpost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptrevenueregistered | Use this endpoint when a receipt revenue has been registered and the corresponding bookings are to be created.
*AccountingApi* | [**apiV1InternalAccountingBookingAccountingeventReceiptrevenueregisteredValidatePost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingaccountingeventreceiptrevenueregisteredvalidatepost) | **POST** /api/v1/_internal/accounting/booking/accountingevent/receiptrevenueregistered/validate | Use this endpoint when validating a revenue receipt before registering it.
*AccountingApi* | [**apiV1InternalAccountingBookingAllpaymentsdetachedDelete**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingallpaymentsdetacheddelete) | **DELETE** /api/v1/_internal/accounting/booking/allpaymentsdetached | Use this endpoint when a document has been set to status &#39;open&#39;.
*AccountingApi* | [**apiV1InternalAccountingBookingCompanysettingsBydocumentidandtypeGet**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingcompanysettingsbydocumentidandtypeget) | **GET** /api/v1/_internal/accounting/booking/companysettings/bydocumentidandtype | Use this endpoint to get the company settings (taxation type and profit assessment type) used for the sent event booking of a given document.
*AccountingApi* | [**apiV1InternalAccountingBookingCompanysettingsDocumentsGet**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingcompanysettingsdocumentsget) | **GET** /api/v1/_internal/accounting/booking/companysettings/documents | Use this endpoint to check if a change of company settings is possbile.
*AccountingApi* | [**apiV1InternalAccountingBookingCompanysettingsRebookPost**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingcompanysettingsrebookpost) | **POST** /api/v1/_internal/accounting/booking/companysettings/rebook | Use this endpoint to trigger the rebooking of relevant bookings when performing a company setting change.
*AccountingApi* | [**apiV1InternalAccountingBookingDocumentDeletedDelete**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingdocumentdeleteddelete) | **DELETE** /api/v1/_internal/accounting/booking/document/deleted | Use this endpoint when a document has been deleted and the sent event bookings have to be deleted.
*AccountingApi* | [**apiV1InternalAccountingBookingDocumentEditedDelete**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingdocumentediteddelete) | **DELETE** /api/v1/_internal/accounting/booking/document/edited | Use this endpoint when a document has been edited and the sent event bookings have to be deleted.
*AccountingApi* | [**apiV1InternalAccountingBookingPaymentdetachedDelete**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingpaymentdetacheddelete) | **DELETE** /api/v1/_internal/accounting/booking/paymentdetached | Use this endpoint when a transaction has been detached from a document.
*AccountingApi* | [**apiV1InternalAccountingBookingResetDelete**](docs/Api/AccountingApi.md#apiv1internalaccountingbookingresetdelete) | **DELETE** /api/v1/_internal/accounting/booking/reset | Use this endpoint to delete all booking data of a client.
*ClientLockApi* | [**apiV1InternalRebookingStatusGet**](docs/Api/ClientLockApi.md#apiv1internalrebookingstatusget) | **GET** /api/v1/_internal/rebooking/status | This endpoint is used to receive the status of a client lock for a specific client.
*DatevApi* | [**apiV1InternalAccountingDatevBookingEurBydocumentreferencesPost**](docs/Api/DatevApi.md#apiv1internalaccountingdatevbookingeurbydocumentreferencespost) | **POST** /api/v1/_internal/accounting/datev/booking/eur/bydocumentreferences | Returns all bookings and accounting records grouped by document reference with the relevant information which is used for the export to DATEV for a client in the given time range.
*DatevApi* | [**apiV1InternalAccountingDatevBookingEurBypaymentGet**](docs/Api/DatevApi.md#apiv1internalaccountingdatevbookingeurbypaymentget) | **GET** /api/v1/_internal/accounting/datev/booking/eur/bypayment | Returns all payment bookings and accounting records with the relevant information which is used for the export to DATEV for a client in the given time range.
*DatevApi* | [**apiV1InternalAccountingDatevBookingGet**](docs/Api/DatevApi.md#apiv1internalaccountingdatevbookingget) | **GET** /api/v1/_internal/accounting/datev/booking | Returns all payment bookings and accounting records with the relevant information which is used for the export to DATEV for a client in the given time range. Revenue bookings are loaded using document date, payment bookings are loaded using the pay date.
*EnshrineApi* | [**apiV1InternalBookkeepingBookingEnshrineDaterangePatch**](docs/Api/EnshrineApi.md#apiv1internalbookkeepingbookingenshrinedaterangepatch) | **PATCH** /api/v1/_internal/bookkeeping/booking/enshrine/daterange | Use this endpoint when bookings of documents, attachments and payments are to be enshrined by date range
*EnshrineApi* | [**apiV1InternalBookkeepingBookingEnshrineDocumentsPatch**](docs/Api/EnshrineApi.md#apiv1internalbookkeepingbookingenshrinedocumentspatch) | **PATCH** /api/v1/_internal/bookkeeping/booking/enshrine/documents | Use this endpoint when bookings of documents, attachments and payments are to be enshrined
*ReportApi* | [**apiV1InternalAccountingReportUstvaGet**](docs/Api/ReportApi.md#apiv1internalaccountingreportustvaget) | **GET** /api/v1/_internal/accounting/report/ustva | Returns the UStVA report for given period.
*ReportApi* | [**apiV1InternalAccountingReportUstvaKzfieldsGet**](docs/Api/ReportApi.md#apiv1internalaccountingreportustvakzfieldsget) | **GET** /api/v1/_internal/accounting/report/ustva/kzfields | Returns kz field numbers grouped by the ustva row.
*ReportApi* | [**apiV1InternalReportBwaExportGet**](docs/Api/ReportApi.md#apiv1internalreportbwaexportget) | **GET** /api/v1/_internal/report/bwa/export | Returns the BWA as PDF for the given period.
*ReportApi* | [**apiV1InternalReportBwaExportPost**](docs/Api/ReportApi.md#apiv1internalreportbwaexportpost) | **POST** /api/v1/_internal/report/bwa/export | Returns the BWA as PDF for the given period, filtered by documents.
*ReportApi* | [**apiV1InternalReportBwaGet**](docs/Api/ReportApi.md#apiv1internalreportbwaget) | **GET** /api/v1/_internal/report/bwa | Returns the BWA report for the given period.
*ReportApi* | [**apiV1InternalReportBwaPost**](docs/Api/ReportApi.md#apiv1internalreportbwapost) | **POST** /api/v1/_internal/report/bwa | Returns the BWA report for the given period, filtered by documents.
*ReportApi* | [**apiV1InternalReportComparativebwaGet**](docs/Api/ReportApi.md#apiv1internalreportcomparativebwaget) | **GET** /api/v1/_internal/report/comparativebwa | Returns the comparative BWA report for the given period.
*ReportApi* | [**apiV1InternalReportComparativebwaPost**](docs/Api/ReportApi.md#apiv1internalreportcomparativebwapost) | **POST** /api/v1/_internal/report/comparativebwa | Returns the comparative BWA report for the given period, filtered by documents.
*RulemasterApi* | [**apiV1InternalAccountingBookingAccountingrecordDocumentGet**](docs/Api/RulemasterApi.md#apiv1internalaccountingbookingaccountingrecorddocumentget) | **GET** /api/v1/_internal/accounting/booking/accountingrecord/document | Use this endpoint to get all accounting records of a document.
*RulemasterApi* | [**apiV1InternalAccountingReportRulemasterUstvaGet**](docs/Api/RulemasterApi.md#apiv1internalaccountingreportrulemasterustvaget) | **GET** /api/v1/_internal/accounting/report/rulemaster/ustva | Returns the UStVA report for given period as rulemaster including document relations per category. This is only internal functionality for QA not for customer.
*RulemasterApi* | [**apiV1InternalBookkeepingBookingImmutabilityReversedDaterangePatch**](docs/Api/RulemasterApi.md#apiv1internalbookkeepingbookingimmutabilityreverseddaterangepatch) | **PATCH** /api/v1/_internal/bookkeeping/booking/immutability/reversed/daterange | Use this endpoint when the immutability of bookings of documents and payments have to be reversed by date range
*RulemasterApi* | [**apiV1InternalBookkeepingBookingImmutabilityReversedDocumentsPatch**](docs/Api/RulemasterApi.md#apiv1internalbookkeepingbookingimmutabilityreverseddocumentspatch) | **PATCH** /api/v1/_internal/bookkeeping/booking/immutability/reversed/documents | Use this endpoint when the immutability of bookings of documents and payments have to be reversed.
*RulemasterApi* | [**apiV1InternalRebookingDocumentTypeDocumentIdGet**](docs/Api/RulemasterApi.md#apiv1internalrebookingdocumenttypedocumentidget) | **GET** /api/v1/_internal/rebooking/{documentType}/{documentId} | This endpoint is used to trigger a rebooking for a specific document.
*RulemasterApi* | [**findAllBookingKeysBy**](docs/Api/RulemasterApi.md#findallbookingkeysby) | **GET** /api/v1/_internal/accounting/booking/rulemaster/bookingkey | Use this endpoint to fetch all Rulemaster Booking Keys for display in View
*VatApi* | [**getAllowedTaxRatesBy**](docs/Api/VatApi.md#getallowedtaxratesby) | **GET** /api/v1/_internal/accounting/countryvatrates | Retrieve valid EU VAT rates for a specific country and date

## Models

- [AccountDatev](docs/Model/AccountDatev.md)
- [AccountDatevBookingKey](docs/Model/AccountDatevBookingKey.md)
- [AccountDatevExternalInner](docs/Model/AccountDatevExternalInner.md)
- [AccountDatevExternalTaxRules](docs/Model/AccountDatevExternalTaxRules.md)
- [AccountType](docs/Model/AccountType.md)
- [AccountValidTaxRuleInformation](docs/Model/AccountValidTaxRuleInformation.md)
- [AccountingContact](docs/Model/AccountingContact.md)
- [AccountingEventConflictErrorResponse](docs/Model/AccountingEventConflictErrorResponse.md)
- [AccountingEventNoMatchingRuleFoundErrorResponse](docs/Model/AccountingEventNoMatchingRuleFoundErrorResponse.md)
- [AccountingEventResponse](docs/Model/AccountingEventResponse.md)
- [AccountingEventResponseNoBooking](docs/Model/AccountingEventResponseNoBooking.md)
- [AccountingEventValidationInvalidPayloadErrorResponse](docs/Model/AccountingEventValidationInvalidPayloadErrorResponse.md)
- [AccountingEventValidationResponse](docs/Model/AccountingEventValidationResponse.md)
- [AccountingProfitDeterminationType](docs/Model/AccountingProfitDeterminationType.md)
- [AdvanceInvoicePaidEvent](docs/Model/AdvanceInvoicePaidEvent.md)
- [AdvanceInvoiceResponseNoBooking](docs/Model/AdvanceInvoiceResponseNoBooking.md)
- [AdvanceInvoiceSentEvent](docs/Model/AdvanceInvoiceSentEvent.md)
- [AllPaymentsDetachedEvent](docs/Model/AllPaymentsDetachedEvent.md)
- [ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest](docs/Model/ApiV1InternalAccountingBookingAccountingeventBanktransactionBookPostRequest.md)
- [ApiV1InternalReportComparativebwaGet200Response](docs/Model/ApiV1InternalReportComparativebwaGet200Response.md)
- [ApiV1InternalReportComparativebwaGet200Response1](docs/Model/ApiV1InternalReportComparativebwaGet200Response1.md)
- [AssetAccountBoundaries](docs/Model/AssetAccountBoundaries.md)
- [AssetAccountBoundariesAmountLimits](docs/Model/AssetAccountBoundariesAmountLimits.md)
- [AssetAccountGuide](docs/Model/AssetAccountGuide.md)
- [AssetAccountGuideAllOf](docs/Model/AssetAccountGuideAllOf.md)
- [BankTransaction](docs/Model/BankTransaction.md)
- [BankTransactionEvent](docs/Model/BankTransactionEvent.md)
- [BankTransactionReverseEvent](docs/Model/BankTransactionReverseEvent.md)
- [BookingKey](docs/Model/BookingKey.md)
- [BookingKeyTaxRate](docs/Model/BookingKeyTaxRate.md)
- [BookingReversedEvent](docs/Model/BookingReversedEvent.md)
- [BookkeepingDeleteServerError](docs/Model/BookkeepingDeleteServerError.md)
- [BookkeepingNotAllowedError](docs/Model/BookkeepingNotAllowedError.md)
- [BwaCompareValues](docs/Model/BwaCompareValues.md)
- [BwaDocumentRef](docs/Model/BwaDocumentRef.md)
- [BwaRow](docs/Model/BwaRow.md)
- [CancellationInvoiceSentEvent](docs/Model/CancellationInvoiceSentEvent.md)
- [ChartOfAccounts](docs/Model/ChartOfAccounts.md)
- [Client](docs/Model/Client.md)
- [ClientLockStatus](docs/Model/ClientLockStatus.md)
- [ClientLockStatusResponse](docs/Model/ClientLockStatusResponse.md)
- [CompanySettingRebookingParameters](docs/Model/CompanySettingRebookingParameters.md)
- [CompanySettingsEventResponse](docs/Model/CompanySettingsEventResponse.md)
- [CompanySettingsEventResponseValue](docs/Model/CompanySettingsEventResponseValue.md)
- [ComparativeBwaRow](docs/Model/ComparativeBwaRow.md)
- [CountryCodeEnum](docs/Model/CountryCodeEnum.md)
- [CreditNote](docs/Model/CreditNote.md)
- [CreditNoteNegativePosition](docs/Model/CreditNoteNegativePosition.md)
- [CreditNotePaidEvent](docs/Model/CreditNotePaidEvent.md)
- [CreditNotePosition](docs/Model/CreditNotePosition.md)
- [CreditNotePositionWithClaim](docs/Model/CreditNotePositionWithClaim.md)
- [CreditNotePositionsInner](docs/Model/CreditNotePositionsInner.md)
- [CreditNotePositivePosition](docs/Model/CreditNotePositivePosition.md)
- [CreditNoteSentEvent](docs/Model/CreditNoteSentEvent.md)
- [CreditNoteUnderachievement](docs/Model/CreditNoteUnderachievement.md)
- [CreditNoteUnderachievementNegativePosition](docs/Model/CreditNoteUnderachievementNegativePosition.md)
- [CreditNoteUnderachievementPaidEvent](docs/Model/CreditNoteUnderachievementPaidEvent.md)
- [CreditNoteUnderachievementPositionsInner](docs/Model/CreditNoteUnderachievementPositionsInner.md)
- [CreditNoteUnderachievementPositivePosition](docs/Model/CreditNoteUnderachievementPositivePosition.md)
- [CreditNoteUnderachievementSentEvent](docs/Model/CreditNoteUnderachievementSentEvent.md)
- [Creditor](docs/Model/Creditor.md)
- [DatevAccount](docs/Model/DatevAccount.md)
- [DatevAccountingRecord](docs/Model/DatevAccountingRecord.md)
- [DatevAutomaticAccountingRecord](docs/Model/DatevAutomaticAccountingRecord.md)
- [DatevBooking](docs/Model/DatevBooking.md)
- [DatevBookingClient](docs/Model/DatevBookingClient.md)
- [DatevBookingDocumentRef](docs/Model/DatevBookingDocumentRef.md)
- [DatevBookingsResponse](docs/Model/DatevBookingsResponse.md)
- [Debitor](docs/Model/Debitor.md)
- [Depreciation](docs/Model/Depreciation.md)
- [DepreciationCorrectionEvent](docs/Model/DepreciationCorrectionEvent.md)
- [DepreciationEvent](docs/Model/DepreciationEvent.md)
- [DocumentAccountingRecordPosition](docs/Model/DocumentAccountingRecordPosition.md)
- [DocumentAccountingRecordPositionAutomaticAccountingRecord](docs/Model/DocumentAccountingRecordPositionAutomaticAccountingRecord.md)
- [DocumentAccountingRecordResult](docs/Model/DocumentAccountingRecordResult.md)
- [DocumentBookingResponse](docs/Model/DocumentBookingResponse.md)
- [DocumentBookingResponseValue](docs/Model/DocumentBookingResponseValue.md)
- [DocumentDeletedEvent](docs/Model/DocumentDeletedEvent.md)
- [DocumentEditedEvent](docs/Model/DocumentEditedEvent.md)
- [DocumentRef](docs/Model/DocumentRef.md)
- [DocumentType](docs/Model/DocumentType.md)
- [DomainErrorCodes](docs/Model/DomainErrorCodes.md)
- [EnshrineBookingsDateRange](docs/Model/EnshrineBookingsDateRange.md)
- [EnshrineDocumentRef](docs/Model/EnshrineDocumentRef.md)
- [EnshrineDocuments](docs/Model/EnshrineDocuments.md)
- [EnshrineGeneralErrorResponse](docs/Model/EnshrineGeneralErrorResponse.md)
- [EnshrineInvalidInputErrorResponse](docs/Model/EnshrineInvalidInputErrorResponse.md)
- [EnshrineResponse](docs/Model/EnshrineResponse.md)
- [FinalInvoicePaid](docs/Model/FinalInvoicePaid.md)
- [FinalInvoicePaidEvent](docs/Model/FinalInvoicePaidEvent.md)
- [FinalInvoicePaidPositionsInner](docs/Model/FinalInvoicePaidPositionsInner.md)
- [FinalInvoiceSent](docs/Model/FinalInvoiceSent.md)
- [FinalInvoiceSentEvent](docs/Model/FinalInvoiceSentEvent.md)
- [GeneralErrorResponse](docs/Model/GeneralErrorResponse.md)
- [Guidance](docs/Model/Guidance.md)
- [GuidanceAccountGuidesInner](docs/Model/GuidanceAccountGuidesInner.md)
- [InvalidInputErrorResponse](docs/Model/InvalidInputErrorResponse.md)
- [InvoicePaid](docs/Model/InvoicePaid.md)
- [InvoicePaidEvent](docs/Model/InvoicePaidEvent.md)
- [InvoicePaidPositionsInner](docs/Model/InvoicePaidPositionsInner.md)
- [InvoiceSent](docs/Model/InvoiceSent.md)
- [InvoiceSentEvent](docs/Model/InvoiceSentEvent.md)
- [KzFieldGroup](docs/Model/KzFieldGroup.md)
- [KzFieldResponse](docs/Model/KzFieldResponse.md)
- [KzFieldResponseKzFieldGroupsInner](docs/Model/KzFieldResponseKzFieldGroupsInner.md)
- [MoneyTransitDepositEvent](docs/Model/MoneyTransitDepositEvent.md)
- [MoneyTransitWithdrawalEvent](docs/Model/MoneyTransitWithdrawalEvent.md)
- [NoBookingsForDocumentErrorResponse](docs/Model/NoBookingsForDocumentErrorResponse.md)
- [NoCompanySettingsFound](docs/Model/NoCompanySettingsFound.md)
- [NotAllowedErrorResponse](docs/Model/NotAllowedErrorResponse.md)
- [NotEnshrinedDocuments](docs/Model/NotEnshrinedDocuments.md)
- [OriginDocument](docs/Model/OriginDocument.md)
- [PaymentAccountsInner](docs/Model/PaymentAccountsInner.md)
- [PaymentDetachedEvent](docs/Model/PaymentDetachedEvent.md)
- [PaymentDifference](docs/Model/PaymentDifference.md)
- [PaymentDifferenceReason](docs/Model/PaymentDifferenceReason.md)
- [PrivateDepositEvent](docs/Model/PrivateDepositEvent.md)
- [PrivateWithdrawalEvent](docs/Model/PrivateWithdrawalEvent.md)
- [ReceiptCreditNotePosition](docs/Model/ReceiptCreditNotePosition.md)
- [ReceiptEntertainment](docs/Model/ReceiptEntertainment.md)
- [ReceiptEntertainmentPaidEvent](docs/Model/ReceiptEntertainmentPaidEvent.md)
- [ReceiptEntertainmentPosition](docs/Model/ReceiptEntertainmentPosition.md)
- [ReceiptEntertainmentRegisteredEvent](docs/Model/ReceiptEntertainmentRegisteredEvent.md)
- [ReceiptExpensePaidEvent](docs/Model/ReceiptExpensePaidEvent.md)
- [ReceiptExpenseRegisteredEvent](docs/Model/ReceiptExpenseRegisteredEvent.md)
- [ReceiptInversePosition](docs/Model/ReceiptInversePosition.md)
- [ReceiptPaid](docs/Model/ReceiptPaid.md)
- [ReceiptPaidPositionsInner](docs/Model/ReceiptPaidPositionsInner.md)
- [ReceiptPosition](docs/Model/ReceiptPosition.md)
- [ReceiptRegistered](docs/Model/ReceiptRegistered.md)
- [ReceiptRegisteredPositionsInner](docs/Model/ReceiptRegisteredPositionsInner.md)
- [ReceiptRevenuePaidEvent](docs/Model/ReceiptRevenuePaidEvent.md)
- [ReceiptRevenueRegisteredEvent](docs/Model/ReceiptRevenueRegisteredEvent.md)
- [ReceiptTaxCreditNotePosition](docs/Model/ReceiptTaxCreditNotePosition.md)
- [ReceiptTaxInversePosition](docs/Model/ReceiptTaxInversePosition.md)
- [ReceiptTaxPosition](docs/Model/ReceiptTaxPosition.md)
- [RegularAccountGuide](docs/Model/RegularAccountGuide.md)
- [RepayLoanEvent](docs/Model/RepayLoanEvent.md)
- [ResetResultDto](docs/Model/ResetResultDto.md)
- [RevenuePosition](docs/Model/RevenuePosition.md)
- [RevenuePositionWithClaim](docs/Model/RevenuePositionWithClaim.md)
- [RowWithBothTaxableBaseAndTaxFields](docs/Model/RowWithBothTaxableBaseAndTaxFields.md)
- [RowWithOnlyOneTaxField](docs/Model/RowWithOnlyOneTaxField.md)
- [RowWithOnlyOneTaxableBaseField](docs/Model/RowWithOnlyOneTaxableBaseField.md)
- [RuleMasterBookingKey](docs/Model/RuleMasterBookingKey.md)
- [SkrAccount](docs/Model/SkrAccount.md)
- [SkrMappingResult](docs/Model/SkrMappingResult.md)
- [TakeLoanEvent](docs/Model/TakeLoanEvent.md)
- [TaxRateEnum](docs/Model/TaxRateEnum.md)
- [TaxRule](docs/Model/TaxRule.md)
- [TaxationScope](docs/Model/TaxationScope.md)
- [TaxationType](docs/Model/TaxationType.md)
- [Totals](docs/Model/Totals.md)
- [TotalsWithClaim](docs/Model/TotalsWithClaim.md)
- [Transaction](docs/Model/Transaction.md)
- [TwoRowsWithOneTaxableBaseAndTwoTaxFields](docs/Model/TwoRowsWithOneTaxableBaseAndTwoTaxFields.md)
- [VatRate](docs/Model/VatRate.md)

## Authorization

### jwtAuthentication

- **Type**: Bearer authentication (JWT)

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author

team_ap@sevdesk.de

## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `20.0.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
