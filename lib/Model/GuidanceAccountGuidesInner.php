<?php
/**
 * GuidanceAccountGuidesInner
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Accounting - Bookkeeping API
 *
 * Welcome to the Bookkeeping API of Team Plumbus
 *
 * The version of the OpenAPI document: 20.0.0
 * Contact: team_ap@sevdesk.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.2.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;

use \ArrayAccess;
use \OpenAPI\Client\ObjectSerializer;

/**
 * GuidanceAccountGuidesInner Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class GuidanceAccountGuidesInner implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = 'accountGuideType';

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'Guidance_accountGuides_inner';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'accountGuideType' => 'string',
        'accountId' => 'int',
        'accountNumber' => 'int',
        'accountName' => 'string',
        'accountDescription' => 'string',
        'chartOfAccounts' => '\OpenAPI\Client\Model\ChartOfAccounts',
        'accountCategory' => 'int',
        'accountType' => '\OpenAPI\Client\Model\AccountType',
        'defaultVisibility' => 'string',
        'allowedDocumentTypes' => 'string[]',
        'allowedTaxRules' => '\OpenAPI\Client\Model\TaxRule[]',
        'allowedPaymentDifferenceReasons' => '\OpenAPI\Client\Model\PaymentDifferenceReason[]',
        'assetAccountBoundaries' => '\OpenAPI\Client\Model\AssetAccountBoundaries[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'accountGuideType' => null,
        'accountId' => null,
        'accountNumber' => null,
        'accountName' => null,
        'accountDescription' => null,
        'chartOfAccounts' => null,
        'accountCategory' => null,
        'accountType' => null,
        'defaultVisibility' => null,
        'allowedDocumentTypes' => null,
        'allowedTaxRules' => null,
        'allowedPaymentDifferenceReasons' => null,
        'assetAccountBoundaries' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'accountGuideType' => false,
		'accountId' => false,
		'accountNumber' => false,
		'accountName' => false,
		'accountDescription' => false,
		'chartOfAccounts' => false,
		'accountCategory' => true,
		'accountType' => false,
		'defaultVisibility' => false,
		'allowedDocumentTypes' => false,
		'allowedTaxRules' => false,
		'allowedPaymentDifferenceReasons' => false,
		'assetAccountBoundaries' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'accountGuideType' => 'accountGuideType',
        'accountId' => 'accountId',
        'accountNumber' => 'accountNumber',
        'accountName' => 'accountName',
        'accountDescription' => 'accountDescription',
        'chartOfAccounts' => 'chartOfAccounts',
        'accountCategory' => 'accountCategory',
        'accountType' => 'accountType',
        'defaultVisibility' => 'defaultVisibility',
        'allowedDocumentTypes' => 'allowedDocumentTypes',
        'allowedTaxRules' => 'allowedTaxRules',
        'allowedPaymentDifferenceReasons' => 'allowedPaymentDifferenceReasons',
        'assetAccountBoundaries' => 'assetAccountBoundaries'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'accountGuideType' => 'setAccountGuideType',
        'accountId' => 'setAccountId',
        'accountNumber' => 'setAccountNumber',
        'accountName' => 'setAccountName',
        'accountDescription' => 'setAccountDescription',
        'chartOfAccounts' => 'setChartOfAccounts',
        'accountCategory' => 'setAccountCategory',
        'accountType' => 'setAccountType',
        'defaultVisibility' => 'setDefaultVisibility',
        'allowedDocumentTypes' => 'setAllowedDocumentTypes',
        'allowedTaxRules' => 'setAllowedTaxRules',
        'allowedPaymentDifferenceReasons' => 'setAllowedPaymentDifferenceReasons',
        'assetAccountBoundaries' => 'setAssetAccountBoundaries'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'accountGuideType' => 'getAccountGuideType',
        'accountId' => 'getAccountId',
        'accountNumber' => 'getAccountNumber',
        'accountName' => 'getAccountName',
        'accountDescription' => 'getAccountDescription',
        'chartOfAccounts' => 'getChartOfAccounts',
        'accountCategory' => 'getAccountCategory',
        'accountType' => 'getAccountType',
        'defaultVisibility' => 'getDefaultVisibility',
        'allowedDocumentTypes' => 'getAllowedDocumentTypes',
        'allowedTaxRules' => 'getAllowedTaxRules',
        'allowedPaymentDifferenceReasons' => 'getAllowedPaymentDifferenceReasons',
        'assetAccountBoundaries' => 'getAssetAccountBoundaries'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    public const ACCOUNT_GUIDE_TYPE_REGULAR = 'REGULAR';
    public const ACCOUNT_GUIDE_TYPE_ASSET = 'ASSET';
    public const DEFAULT_VISIBILITY_VISIBLE = 'VISIBLE';
    public const DEFAULT_VISIBILITY_HIDDEN = 'HIDDEN';
    public const ALLOWED_DOCUMENT_TYPES_INVOICE = 'INVOICE';
    public const ALLOWED_DOCUMENT_TYPES_ADVANCE_INVOICE = 'ADVANCE_INVOICE';
    public const ALLOWED_DOCUMENT_TYPES_FINAL_INVOICE = 'FINAL_INVOICE';
    public const ALLOWED_DOCUMENT_TYPES_CANCELLATION_INVOICE = 'CANCELLATION_INVOICE';
    public const ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_PROVISION = 'CREDIT_NOTE_PROVISION';
    public const ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_ROYALTY_ASSIGNED = 'CREDIT_NOTE_ROYALTY_ASSIGNED';
    public const ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_ROYALTY_UNASSIGNED = 'CREDIT_NOTE_ROYALTY_UNASSIGNED';
    public const ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_BOOKING_ACCOUNT = 'CREDIT_NOTE_BOOKING_ACCOUNT';
    public const ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_UNDERACHIEVEMENT = 'CREDIT_NOTE_UNDERACHIEVEMENT';
    public const ALLOWED_DOCUMENT_TYPES_RECEIPT_REVENUE = 'RECEIPT_REVENUE';
    public const ALLOWED_DOCUMENT_TYPES_RECEIPT_EXPENSE = 'RECEIPT_EXPENSE';
    public const ALLOWED_DOCUMENT_TYPES_RECEIPT_REVENUE_INVERSE = 'RECEIPT_REVENUE_INVERSE';
    public const ALLOWED_DOCUMENT_TYPES_RECEIPT_EXPENSE_INVERSE = 'RECEIPT_EXPENSE_INVERSE';
    public const ALLOWED_DOCUMENT_TYPES_RECEIPT_ENTERTAINMENT = 'RECEIPT_ENTERTAINMENT';
    public const ALLOWED_DOCUMENT_TYPES_RECEIPT_TAX = 'RECEIPT_TAX';
    public const ALLOWED_DOCUMENT_TYPES_RECEIPT_TAX_INVERSE = 'RECEIPT_TAX_INVERSE';
    public const ALLOWED_DOCUMENT_TYPES_BANK_TRANSACTION = 'BANK_TRANSACTION';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getAccountGuideTypeAllowableValues()
    {
        return [
            self::ACCOUNT_GUIDE_TYPE_REGULAR,
            self::ACCOUNT_GUIDE_TYPE_ASSET,
        ];
    }

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getDefaultVisibilityAllowableValues()
    {
        return [
            self::DEFAULT_VISIBILITY_VISIBLE,
            self::DEFAULT_VISIBILITY_HIDDEN,
        ];
    }

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getAllowedDocumentTypesAllowableValues()
    {
        return [
            self::ALLOWED_DOCUMENT_TYPES_INVOICE,
            self::ALLOWED_DOCUMENT_TYPES_ADVANCE_INVOICE,
            self::ALLOWED_DOCUMENT_TYPES_FINAL_INVOICE,
            self::ALLOWED_DOCUMENT_TYPES_CANCELLATION_INVOICE,
            self::ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_PROVISION,
            self::ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_ROYALTY_ASSIGNED,
            self::ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_ROYALTY_UNASSIGNED,
            self::ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_BOOKING_ACCOUNT,
            self::ALLOWED_DOCUMENT_TYPES_CREDIT_NOTE_UNDERACHIEVEMENT,
            self::ALLOWED_DOCUMENT_TYPES_RECEIPT_REVENUE,
            self::ALLOWED_DOCUMENT_TYPES_RECEIPT_EXPENSE,
            self::ALLOWED_DOCUMENT_TYPES_RECEIPT_REVENUE_INVERSE,
            self::ALLOWED_DOCUMENT_TYPES_RECEIPT_EXPENSE_INVERSE,
            self::ALLOWED_DOCUMENT_TYPES_RECEIPT_ENTERTAINMENT,
            self::ALLOWED_DOCUMENT_TYPES_RECEIPT_TAX,
            self::ALLOWED_DOCUMENT_TYPES_RECEIPT_TAX_INVERSE,
            self::ALLOWED_DOCUMENT_TYPES_BANK_TRANSACTION,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('accountGuideType', $data ?? [], null);
        $this->setIfExists('accountId', $data ?? [], null);
        $this->setIfExists('accountNumber', $data ?? [], null);
        $this->setIfExists('accountName', $data ?? [], null);
        $this->setIfExists('accountDescription', $data ?? [], null);
        $this->setIfExists('chartOfAccounts', $data ?? [], null);
        $this->setIfExists('accountCategory', $data ?? [], null);
        $this->setIfExists('accountType', $data ?? [], null);
        $this->setIfExists('defaultVisibility', $data ?? [], null);
        $this->setIfExists('allowedDocumentTypes', $data ?? [], null);
        $this->setIfExists('allowedTaxRules', $data ?? [], null);
        $this->setIfExists('allowedPaymentDifferenceReasons', $data ?? [], null);
        $this->setIfExists('assetAccountBoundaries', $data ?? [], null);

        // Initialize discriminator property with the model name.
        $this->container['accountGuideType'] = static::$openAPIModelName;
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getAccountGuideTypeAllowableValues();
        if (!is_null($this->container['accountGuideType']) && !in_array($this->container['accountGuideType'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value '%s' for 'accountGuideType', must be one of '%s'",
                $this->container['accountGuideType'],
                implode("', '", $allowedValues)
            );
        }

        if ($this->container['accountId'] === null) {
            $invalidProperties[] = "'accountId' can't be null";
        }
        if ($this->container['accountNumber'] === null) {
            $invalidProperties[] = "'accountNumber' can't be null";
        }
        if ($this->container['accountName'] === null) {
            $invalidProperties[] = "'accountName' can't be null";
        }
        if ($this->container['chartOfAccounts'] === null) {
            $invalidProperties[] = "'chartOfAccounts' can't be null";
        }
        if ($this->container['accountCategory'] === null) {
            $invalidProperties[] = "'accountCategory' can't be null";
        }
        if ($this->container['accountType'] === null) {
            $invalidProperties[] = "'accountType' can't be null";
        }
        if ($this->container['defaultVisibility'] === null) {
            $invalidProperties[] = "'defaultVisibility' can't be null";
        }
        $allowedValues = $this->getDefaultVisibilityAllowableValues();
        if (!is_null($this->container['defaultVisibility']) && !in_array($this->container['defaultVisibility'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value '%s' for 'defaultVisibility', must be one of '%s'",
                $this->container['defaultVisibility'],
                implode("', '", $allowedValues)
            );
        }

        if ($this->container['allowedDocumentTypes'] === null) {
            $invalidProperties[] = "'allowedDocumentTypes' can't be null";
        }
        if ($this->container['allowedTaxRules'] === null) {
            $invalidProperties[] = "'allowedTaxRules' can't be null";
        }
        if ($this->container['allowedPaymentDifferenceReasons'] === null) {
            $invalidProperties[] = "'allowedPaymentDifferenceReasons' can't be null";
        }
        if ($this->container['assetAccountBoundaries'] === null) {
            $invalidProperties[] = "'assetAccountBoundaries' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets accountGuideType
     *
     * @return string|null
     */
    public function getAccountGuideType()
    {
        return $this->container['accountGuideType'];
    }

    /**
     * Sets accountGuideType
     *
     * @param string|null $accountGuideType accountGuideType
     *
     * @return self
     */
    public function setAccountGuideType($accountGuideType)
    {
        $allowedValues = $this->getAccountGuideTypeAllowableValues();
        if (!is_null($accountGuideType) && !in_array($accountGuideType, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'accountGuideType', must be one of '%s'",
                    $accountGuideType,
                    implode("', '", $allowedValues)
                )
            );
        }

        if (is_null($accountGuideType)) {
            throw new \InvalidArgumentException('non-nullable accountGuideType cannot be null');
        }

        $this->container['accountGuideType'] = $accountGuideType;

        return $this;
    }

    /**
     * Gets accountId
     *
     * @return int
     */
    public function getAccountId()
    {
        return $this->container['accountId'];
    }

    /**
     * Sets accountId
     *
     * @param int $accountId accountId
     *
     * @return self
     */
    public function setAccountId($accountId)
    {

        if (is_null($accountId)) {
            throw new \InvalidArgumentException('non-nullable accountId cannot be null');
        }

        $this->container['accountId'] = $accountId;

        return $this;
    }

    /**
     * Gets accountNumber
     *
     * @return int
     */
    public function getAccountNumber()
    {
        return $this->container['accountNumber'];
    }

    /**
     * Sets accountNumber
     *
     * @param int $accountNumber accountNumber
     *
     * @return self
     */
    public function setAccountNumber($accountNumber)
    {

        if (is_null($accountNumber)) {
            throw new \InvalidArgumentException('non-nullable accountNumber cannot be null');
        }

        $this->container['accountNumber'] = $accountNumber;

        return $this;
    }

    /**
     * Gets accountName
     *
     * @return string
     */
    public function getAccountName()
    {
        return $this->container['accountName'];
    }

    /**
     * Sets accountName
     *
     * @param string $accountName accountName
     *
     * @return self
     */
    public function setAccountName($accountName)
    {

        if (is_null($accountName)) {
            throw new \InvalidArgumentException('non-nullable accountName cannot be null');
        }

        $this->container['accountName'] = $accountName;

        return $this;
    }

    /**
     * Gets accountDescription
     *
     * @return string|null
     */
    public function getAccountDescription()
    {
        return $this->container['accountDescription'];
    }

    /**
     * Sets accountDescription
     *
     * @param string|null $accountDescription accountDescription
     *
     * @return self
     */
    public function setAccountDescription($accountDescription)
    {

        if (is_null($accountDescription)) {
            throw new \InvalidArgumentException('non-nullable accountDescription cannot be null');
        }

        $this->container['accountDescription'] = $accountDescription;

        return $this;
    }

    /**
     * Gets chartOfAccounts
     *
     * @return \OpenAPI\Client\Model\ChartOfAccounts
     */
    public function getChartOfAccounts()
    {
        return $this->container['chartOfAccounts'];
    }

    /**
     * Sets chartOfAccounts
     *
     * @param \OpenAPI\Client\Model\ChartOfAccounts $chartOfAccounts chartOfAccounts
     *
     * @return self
     */
    public function setChartOfAccounts($chartOfAccounts)
    {

        if (is_null($chartOfAccounts)) {
            throw new \InvalidArgumentException('non-nullable chartOfAccounts cannot be null');
        }

        $this->container['chartOfAccounts'] = $chartOfAccounts;

        return $this;
    }

    /**
     * Gets accountCategory
     *
     * @return int
     */
    public function getAccountCategory()
    {
        return $this->container['accountCategory'];
    }

    /**
     * Sets accountCategory
     *
     * @param int $accountCategory accountCategory
     *
     * @return self
     */
    public function setAccountCategory($accountCategory)
    {

        if (is_null($accountCategory)) {
            array_push($this->openAPINullablesSetToNull, 'accountCategory');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('accountCategory', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }

        $this->container['accountCategory'] = $accountCategory;

        return $this;
    }

    /**
     * Gets accountType
     *
     * @return \OpenAPI\Client\Model\AccountType
     */
    public function getAccountType()
    {
        return $this->container['accountType'];
    }

    /**
     * Sets accountType
     *
     * @param \OpenAPI\Client\Model\AccountType $accountType accountType
     *
     * @return self
     */
    public function setAccountType($accountType)
    {

        if (is_null($accountType)) {
            throw new \InvalidArgumentException('non-nullable accountType cannot be null');
        }

        $this->container['accountType'] = $accountType;

        return $this;
    }

    /**
     * Gets defaultVisibility
     *
     * @return string
     */
    public function getDefaultVisibility()
    {
        return $this->container['defaultVisibility'];
    }

    /**
     * Sets defaultVisibility
     *
     * @param string $defaultVisibility defaultVisibility
     *
     * @return self
     */
    public function setDefaultVisibility($defaultVisibility)
    {
        $allowedValues = $this->getDefaultVisibilityAllowableValues();
        if (!in_array($defaultVisibility, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'defaultVisibility', must be one of '%s'",
                    $defaultVisibility,
                    implode("', '", $allowedValues)
                )
            );
        }

        if (is_null($defaultVisibility)) {
            throw new \InvalidArgumentException('non-nullable defaultVisibility cannot be null');
        }

        $this->container['defaultVisibility'] = $defaultVisibility;

        return $this;
    }

    /**
     * Gets allowedDocumentTypes
     *
     * @return string[]
     */
    public function getAllowedDocumentTypes()
    {
        return $this->container['allowedDocumentTypes'];
    }

    /**
     * Sets allowedDocumentTypes
     *
     * @param string[] $allowedDocumentTypes allowedDocumentTypes
     *
     * @return self
     */
    public function setAllowedDocumentTypes($allowedDocumentTypes)
    {
        $allowedValues = $this->getAllowedDocumentTypesAllowableValues();
        if (array_diff($allowedDocumentTypes, $allowedValues)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'allowedDocumentTypes', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }

        if (is_null($allowedDocumentTypes)) {
            throw new \InvalidArgumentException('non-nullable allowedDocumentTypes cannot be null');
        }

        $this->container['allowedDocumentTypes'] = $allowedDocumentTypes;

        return $this;
    }

    /**
     * Gets allowedTaxRules
     *
     * @return \OpenAPI\Client\Model\TaxRule[]
     */
    public function getAllowedTaxRules()
    {
        return $this->container['allowedTaxRules'];
    }

    /**
     * Sets allowedTaxRules
     *
     * @param \OpenAPI\Client\Model\TaxRule[] $allowedTaxRules allowedTaxRules
     *
     * @return self
     */
    public function setAllowedTaxRules($allowedTaxRules)
    {

        if (is_null($allowedTaxRules)) {
            throw new \InvalidArgumentException('non-nullable allowedTaxRules cannot be null');
        }

        $this->container['allowedTaxRules'] = $allowedTaxRules;

        return $this;
    }

    /**
     * Gets allowedPaymentDifferenceReasons
     *
     * @return \OpenAPI\Client\Model\PaymentDifferenceReason[]
     */
    public function getAllowedPaymentDifferenceReasons()
    {
        return $this->container['allowedPaymentDifferenceReasons'];
    }

    /**
     * Sets allowedPaymentDifferenceReasons
     *
     * @param \OpenAPI\Client\Model\PaymentDifferenceReason[] $allowedPaymentDifferenceReasons allowedPaymentDifferenceReasons
     *
     * @return self
     */
    public function setAllowedPaymentDifferenceReasons($allowedPaymentDifferenceReasons)
    {

        if (is_null($allowedPaymentDifferenceReasons)) {
            throw new \InvalidArgumentException('non-nullable allowedPaymentDifferenceReasons cannot be null');
        }

        $this->container['allowedPaymentDifferenceReasons'] = $allowedPaymentDifferenceReasons;

        return $this;
    }

    /**
     * Gets assetAccountBoundaries
     *
     * @return \OpenAPI\Client\Model\AssetAccountBoundaries[]
     */
    public function getAssetAccountBoundaries()
    {
        return $this->container['assetAccountBoundaries'];
    }

    /**
     * Sets assetAccountBoundaries
     *
     * @param \OpenAPI\Client\Model\AssetAccountBoundaries[] $assetAccountBoundaries assetAccountBoundaries
     *
     * @return self
     */
    public function setAssetAccountBoundaries($assetAccountBoundaries)
    {

        if (is_null($assetAccountBoundaries)) {
            throw new \InvalidArgumentException('non-nullable assetAccountBoundaries cannot be null');
        }

        $this->container['assetAccountBoundaries'] = $assetAccountBoundaries;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


