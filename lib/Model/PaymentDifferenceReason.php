<?php
/**
 * PaymentDifferenceReason
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Accounting - Bookkeeping API
 *
 * Welcome to the Bookkeeping API of Team Plumbus
 *
 * The version of the OpenAPI document: 20.0.0
 * Contact: team_ap@sevdesk.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 6.2.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;
use \OpenAPI\Client\ObjectSerializer;

/**
 * PaymentDifferenceReason Class Doc Comment
 *
 * @category Class
 * @description Represents the type of the payment difference:  Allowed Values: - PARTIAL_PAYMENT: irrelevant for accounting (document is partially paid) - CASH_DISCOUNT: affects accounting of positions (underpayment) - OTHER: affects accounting of positions (underpayment) - REMINDER: will be booked separately (over-payment) - COSTS_OF_PAYMENT_SERVICE_PROVIDER: will be booked separately - NONE: no payment difference (constraint: mount must be 0) - COLLECTIVE: document is fully paid (remaining amount can be linked to other documents)
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class PaymentDifferenceReason
{
    /**
     * Possible values of this enum
     */
    public const PARTIAL_PAYMENT = 'PARTIAL_PAYMENT';

    public const CASH_DISCOUNT = 'CASH_DISCOUNT';

    public const OTHER = 'OTHER';

    public const REMINDER = 'REMINDER';

    public const COSTS_OF_PAYMENT_SERVICE_PROVIDER = 'COSTS_OF_PAYMENT_SERVICE_PROVIDER';

    public const NONE = 'NONE';

    public const COLLECTIVE = 'COLLECTIVE';

    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::PARTIAL_PAYMENT,
            self::CASH_DISCOUNT,
            self::OTHER,
            self::REMINDER,
            self::COSTS_OF_PAYMENT_SERVICE_PROVIDER,
            self::NONE,
            self::COLLECTIVE
        ];
    }
}


